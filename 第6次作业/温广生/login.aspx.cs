﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication4
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            string name = TextBox1.Text;
            string pwd = TextBox2.Text;
            string constr = "server=.;uid=sa;pwd=123;database=student_db";
            SqlConnection con = new SqlConnection(constr);
            con.Open();
            string sql = "select * from studentinfo where stu_name=@name and password=@pwd";
            SqlParameter[] pars = {
            new SqlParameter("@name",name),
            new SqlParameter("@pwd",pwd),
            };
            SqlCommand cmd = new SqlCommand(sql,con);
            cmd.Parameters.AddRange(pars);
            int result = cmd.ExecuteNonQuery();
            cmd.ExecuteScalar();
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                Session["username"] = name;
                Response.Redirect("home.aspx");

            }
            else {
                Label1.Text = "登录失败";
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("register.aspx");
        }
    }
}