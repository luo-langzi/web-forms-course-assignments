﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="WebApplication4.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server" class="form-signin">
        <div>
            <h2>登录页面</h2>
            账号:<asp:TextBox CssClass="form-control" ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            密码:<asp:TextBox TextMode="Password" CssClass="form-control" ID="TextBox2" runat="server"></asp:TextBox>
            <br />
            <asp:Button CssClass="btn btn-primary" ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <asp:Button ID="Button2" runat="server" Text="注册" OnClick="Button2_Click" />
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        </div>
    </form>
</body>
</html>
