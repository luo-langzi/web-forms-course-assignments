﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private SQLHelp sqlhelp = new SQLHelp();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string name = TextBox1.Text;
            string password = TextBox2.Text;
            string sql = "selsec * from StudentInfo where stu_name = @name and password = @password";

            SqlParameter[] pars = 
            {
                new SqlParameter("@name",name),
                new SqlParameter("@password", password) 
            };

            DataTable dt = sqlhelp.Get(sql, pars);

            if (dt.Rows.Count > 0)
            {
                Session["CurrenUserName"] = name;
                Response.Redirect("WebForm2.aspx");
            }
            else
            {
                Literal1.Text = "登录失败！";
            }
        }
    }
}