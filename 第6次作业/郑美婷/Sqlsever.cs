﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication3
{
    public class Sqlsever
    {
        private string address = "sever=LAPTOP-CEEAUKFH;database=Userzh_db";
        private SqlConnection scn = null;
        public Sqlsever()
        {
            scn = new SqlConnection(address);
        }
        /// <summary>
        /// 执行调查
        /// </summary>
        public DataTable Get(string sql, SqlParameter[] spam)
        {
            try
            {
                if (scn.State == ConnectionState.Closed) 
                {
                    scn.Open();
                }
                SqlCommand scmd = new SqlCommand(sql,scn);
                if (spam != null) 
                {
                    scmd.Parameters.AddRange(spam);
                }
                //SqlDataAdapter sdapt = new SqlDataAdapter(scmd);
                //DataSet dst = new DataSet();
                //sdapt.Fill(dst);
                //return dst.Tables[0];
                return scmd.ExecuteNonQuery() > 0 ? true : false;
            }
            catch (Exception ex)
            { }
            finally
            {
                if (scn != null) 
                {
                    scn.Close();//释放资源
                }
            }
        }
    }
}