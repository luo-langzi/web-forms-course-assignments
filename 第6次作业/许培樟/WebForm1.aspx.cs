﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            string name = TextBox1.Text;
            string password = TextBox2.Text;
            SqlConnection sqlCt = new SqlConnection();
            sqlCt.ConnectionString = "Data Source = LAPTOP - VRL0O07U; Initial Catalog = Student; Integrated Security = SSPI;";

            sqlCt.Open();

            string sql = "select * from StudentInfo where stuName = @name and password = @password";
            SqlParameter[] pra =
           {
                new SqlParameter("@name",name),
                new SqlParameter("@pwd",password),
            };

            SqlCommand sqlCom = new SqlCommand(sql, sqlCt);

            if (pra != null)
            {
                sqlCom.Parameters.AddRange(pra);
            }

            SqlDataReader sqlData = sqlCom.ExecuteReader();
            if (sqlData.Read())
            {
                Session["CurrentUserName"] = name;
                Label1.Text = "登录成功";
            }
            else
            {
                Label1.Text = "登录失败";
            }

        }
    }
}