﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string name = TextBox1.Text;
            string pwd = TextBox2.Text;

            string server = "server = .;uid = sa;pwd = 123456;database = Student";

            SqlConnection sqlCon = new SqlConnection(server);

            sqlCon.Open();

            string sql = "select * from StudentInfo where stuName = @name and password = @pwd";

            SqlParameter[] pras =
            {
                new SqlParameter("@name",name),
                new SqlParameter("@pwd",pwd),
            };

            SqlCommand sqlCom = new SqlCommand(sql,sqlCon);

            if (pras != null)
            {
                sqlCom.Parameters.AddRange(pras);
            }

            SqlDataReader sqlData = sqlCom.ExecuteReader();
            if (sqlData.Read())
            {
                Session["CurrentUserName"] = name;
                Label1.Text = "登录成功";
            }
            else
            {
                Label1.Text = "登录失败";
            }
        }
    }
}