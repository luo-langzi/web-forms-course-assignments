﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication3
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string name = TextBox1.Text;
            string psw = TextBox2.Text;
            string sql = "select*from StudentInfo where stu_name=@name and password=@psw";
            SqlParameter[] spar =
            {
            new SqlParameter("@name",name),
            new SqlParameter("@psw", psw)
        };
            DataTable dtbl = Sqlsever.Get(sql, spar);
            if (dtbl.Rows.Count > 0) 
            {
                Session["CurrentUserName"] = name;
                Response.Redirect(".aspx");
            }
            else
            {
                Literal1.Text = "登录失败！";
            }
        }
    }
}