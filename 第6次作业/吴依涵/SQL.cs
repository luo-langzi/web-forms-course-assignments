﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication9
{
    public class SQL
    {
        private static string constr = "server=.;uid=sa;pwd=123456;database=Student_db";
        private SqlConnection con = null;

        public SQL()
        {
            con = new SqlConnection(constr);
        }
        public DataTable Get(string sql, SqlParameter[] pars)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlCommand cmd = new SqlCommand(sql, con);
                if (pars != null)
                {
                    cmd.Parameters.AddRange(pars);
                }

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adapter.Fill(ds);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }
        }
        public bool Execute(string sql, SqlParameter[] pars)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlCommand cmd = new SqlCommand(sql, con);
                if (pars != null)
                {
                    cmd.Parameters.AddRange(pars);
                }

                return cmd.ExecuteNonQuery() > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }
        }
    }
}