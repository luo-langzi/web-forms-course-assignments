﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication9.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h2>登录页面</h2>
        <div>
            用户名：<asp:TextBox CssClass="from-control" ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            密码：<asp:TextBox TextMode="Password" CssClass="form-control" ID="TextBox2" runat="server"></asp:TextBox>
            <br />
            <asp:Button CssClass="btn btn-primary btn-block btn-lg" ID="Button1" runat="server" Text="登录" OnClick="Button1_Click"/>
            <br />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
