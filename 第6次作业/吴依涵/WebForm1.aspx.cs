﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication9
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private SQL sQL = new SQL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string name = TextBox1.Text;
            string pwd = TextBox2.Text;

            string sql = "select * from StudentInfo where stu_name=@name and password=@pwd";
            SqlParameter[] pars =
            {
                new SqlParameter("@name",name),
                new SqlParameter("@pwd",pwd)
            };

            DataTable dt = sQL.Get(sql, pars);
            if (dt.Rows.Count > 0)
            {
                Session["CurrentUserName"] = name;
                Response.Redirect("WebForm2.aspx");
            }
            else
            {
                Literal1.Text = "登录失败！";
            }
        }
    }
}