﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication9
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["CurrentUserName"] == null)
            {
                Response.Redirect("WebForm1.aspx");
            }
            else
            {
                Response.Write($"<h2>你好：，{Session["CurrentUserName"]}</h2>");
            }
        }
    }
}