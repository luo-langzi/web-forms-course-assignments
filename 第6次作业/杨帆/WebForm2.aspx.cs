﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            

            if(Session["CurrentUserName"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                string name = (string)Session["CurrentUserName"];
                Response.Write($"<h1>登录成功！{name}欢迎！</h1>");
            }
        }
    }
}