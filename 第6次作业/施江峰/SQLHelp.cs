﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication2
{
    public class SQLHelp
    {
        private static string constr = server=.;uid=sa;pwd=123456;database=Student_db;
        private SqlConnection con = null;
        public SQLHelp() 
        {
            con = new SqlConnection(constr);
        }

        public DataTable Get(string sql, SqlParameter[] pars) 
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand cmd = new SqlCommand(sql, con);

                if (pars != null)
                {
                    cmd.Parameters.AddRange(pars);
                }

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                sda.Fill(ds);

                return ds.Tables[0];
            }
            catch (Exception a)
            {

                throw new Exception(a.Message);
            }
            finally 
            {
                if (con == null)
                {
                    con.Close();
                }
            }

        }
        public bool A(string constr, SqlParameter[] pars) 
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand cmd = new SqlCommand();

                if (pars != null)
                {
                    cmd.Parameters.AddRange(pars);
                }
                return cmd.ExecuteNonQuery()  0  true  false;
            }
            catch (Exception A)
            {

                throw new Exception(A.Message);
            }
            finally 
            {
                if (con != null)
                {
                    con.Close();
                }
            }
        }
    }
}