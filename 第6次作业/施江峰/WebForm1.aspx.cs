﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication4
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string name = TextBox1.Text;
            string pwd = TextBox2.Text;
            string constr = "server=.;uid=;pwd=;database=Student_db";
            SqlConnection con = new SqlConnection(constr);

            con.Open();
            string sql = "select * from StudentInfo where stu_name=@name and password=@pwd";
            SqlParameter[] pars =
            {
                new SqlParameter("@name",name),
                new SqlParameter("@pwd",pwd)
            };
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.AddRange(pars);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                Session["CurrentUserName"] = name;
                Response.Redirect("Home.aspx");
            }
            else
            {
                Literal1.Text = "登录失败！";
            }

        }
    }
}