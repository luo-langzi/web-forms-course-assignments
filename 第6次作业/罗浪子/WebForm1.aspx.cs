﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string name = TextBox1.Text;
            string pwd = TextBox2.Text;

            //Response.Redirect($"Login.aspx?name={name}");
            string constr = "server=.;uid=gao;pwd=123;database=Student_db"; //数据库连接串的编写
            SqlConnection sqlcon = new SqlConnection(constr);  //SqlConnection 类与数据库连接
            sqlcon.Open();
            string sql = "select * from StudentInfo where stu_name=@name and password=@pwd";  //查询的内容 @为标记的内容
            SqlParameter[] pars =
            {
                new SqlParameter("@name",name),
                new SqlParameter("@pwd",pwd)
            };

            SqlCommand cmd = new SqlCommand(sql, sqlcon);
            cmd.Parameters.AddRange(pars);
            SqlDataReader sdr = cmd.ExecuteReader(); //查询
            if (sdr.Read())
            {
                Literal1.Text = "登录成功<a href='Login.aspx'>去看看吧</a>";
                
            }
            else
            {
                Literal1.Text = "登录失败";
            }

        }
    }
}