﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] != null)
            {
                if (Application["chatromm"] == null)
                {
                    Application["chatroom"] = new Panel();
                }
                this.Panel1.Controls.Add((Panel)Application["chatroom"]);
            }
            else
            {
                Response.Redirect("WebForm1.aspx");            
            }



        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string text = TextBox1.Text;
            string username = Session["username"].ToString();
            string time = DateTime.Now.ToString();

            Literal literal = new Literal();
            literal.Text = username + "" + time + " :" + text + "<br/>";
            ((Panel)Application["charroom"]).Controls.Add(literal);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Page_Load(sender, e);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("WebForm.aspx");
        }
    }
}