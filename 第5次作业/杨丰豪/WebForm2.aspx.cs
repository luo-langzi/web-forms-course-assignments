﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication12
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserName"] == null)
            {
                Response.Redirect("WebForm1.aspx");
            }else if (Application["chatRoom"]==null)
            {
                Application["chatRoom"] = new Panel();
            }
            this.Page.Controls.Add((Panel)Application["chatRoom"]);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string name = Session["UserName"].ToString();
            string time = DateTime.Now.ToString();
            string charString = TextBox1.Text;
            Literal literal = new Literal();
            literal.Text = name + " " + time + ":" + charString + "<br/>";
            ((Panel)Application["chatRoom"]).Controls.Add(literal);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Page_Load(sender, e);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("WebForm1.aspx");
        }
    }
}