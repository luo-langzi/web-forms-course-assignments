﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication3.Speaking
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["name"] != null)
            {
                if (Application["Speakroom"] == null)
                {
                    Application["Speakroom"] = new Panel();
                }
                this.Panel1.Controls.Add((Panel)Application["Speakroom"]);
            }
            else
            {
                Response.Redirect("WebForm1.aspx");//回到登陆界面
            }
        }

        protected void Button1_Click(object sender, EventArgs e)//发送
        {
            string text = TextBox1.Text;
            string name = Session["name"].ToString();
            string nowtime = DateTime.Now.ToString();

            Literal literal = new Literal();
            literal.Text = name + "  " + nowtime + " ：" + text + "<br/>";
            ((Panel)Application["Speakroom"]).Controls.Add(literal);
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            Page_Load(sender, e);
        }
        protected void Button3_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("Webform1.aspx");
        }
    }
}