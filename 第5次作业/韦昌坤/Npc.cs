﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp14
{
    class Npc : Me
    {
        private string name1;
        
        public Npc(string name, string name1) : base(name)
        {
            this.Name1 = name1;
            
        }

        public string Name1 { get => name1; set => name1 = value; }

        public int MDF1()
        {   
            Random random = new Random();
            int result = random.Next(1, 4);
            string str = Convert.ToString(result);
            switch (result)
            {
                case 1:
                    str = "剪刀";
                    break;
                case 2:
                    str = "石头";
                    break;
                case 3:
                    str = "布";
                    break;
            }
            Console.WriteLine("{0}出了一个{1}",name1,str);
            return result;
        }
    }
}