﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["name"] != null)
            {
                if (Application["ChatRoom"] == null)
                {
                    Application["ChatRoom"] = new Panel();
                }
                Panel1.Controls.Add((Panel)Application["ChatRoom"]);
            }
            else
            {
                Response.Redirect("WebForm1.aspx");
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string name = (string)Session["name"];
            string text = TextBox1.Text;
            string time = DateTime.Now.ToString();


            Literal literal1 = new Literal();

            literal1.Text = time + " " + name + "：" + "<br />" + text + "<br />" + "<br />";
            ((Panel)Application["ChatRoom"]).Controls.Add(literal1);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Page_Load(sender, e);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("WebForm1.aspx");
        }

        
    }
}