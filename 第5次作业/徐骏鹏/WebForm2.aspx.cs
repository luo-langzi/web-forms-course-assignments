﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo616.ChatDemo
{
    public partial class ChatRoom : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //判断用户名密码是否为空
            if (Session["username"]==null &&Session["password"]==null)
            {
                Response.Redirect("Login.aspx");
            }
            //判断应用变量是否为空
            if (Application["chatRoom"] ==null)
            {
                Application["chatRoom"] = new Panel();
            }
            //获取本页Panel控件 添加应用变量chatRoom
            Panel1.Controls.Add((Control)Application["chatRoom"]);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //获取 姓名，文本内容，当前时间
            string Username = Session["username"].ToString();
            string Text = TextBox1.Text;
            string Time = DateTime.Now.ToString("t");

            //创建label控件
            Literal literal = new Literal();

            //设置literal的内容为姓名 时间 文本
            literal.Text = $"<b>{Username}</b> {Time}<br>{Text}<br>";

            //获取应用变量 强制转换成Panel类型
            Panel p = (Panel)Application["chatRoom"];
            
            //把创建好的literal添加到上一行获取的panel中 
            p.Controls.Add(literal);
        }


        protected void Button3_Click(object sender, EventArgs e)
        {
            Session.Abandon();
        }
    }
}