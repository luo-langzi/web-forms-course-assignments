﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["useName"]==null)
            {
               
                Response.Redirect("WebForm1.aspx");
            }
            else if (Application["chatRoom"] == null)
            {
                Application["ChatRoom"] = new Panel();
            }
             this.Panel1.Controls.Add((Panel)Application["chatRoom"]);
         
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string name = Session["useName"].ToString();
            string time = DateTime.Now.ToShortTimeString();
            string speak = TextBox1.Text;

            Literal literal = new Literal();
           

            
            literal.Text = name + " " + time + " :" + speak + "<br/>";

            ((Panel)Application["ChatRoom"]).Controls.Add(literal);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Page_Load(sender, e);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {

        }
    }
}