﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserName"]==null)
            {
                Response.Redirect("WebForm2.aspx");
            }
            else if(Application["CharRoom"] == null)
            {
                Application["CharRoom"] = new Panel();
            }
            this.Panel1.Controls.Add((Panel)Application["CharRoom"]);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string name = Session["UserName"].ToString();
            string time = DateTime.Now.ToString();
            string Char = TextBox1.Text;
            Literal literal = new Literal();
            literal.Text = name + " " + time + " " + Char+"<br/>";
            ((Panel)(Application["CharRoom"])).Controls.Add(literal);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Page_Load(sender, e);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("WebForm2.aspx");
        }
    }
}