﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["name"]!=null)
            {
                if (Application["room"]==null)
                {

                    Application["room"] = new Panel();

                }
               this.Panel1.Controls.Add((Panel) Application["room"]);


            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string name = Session["name"].ToString();
            string text = TextBox1.Text;
            string time = DateTime.Now.ToString();
            Label label = new Label();
            label.Text = name + "  " + text + "  " + time;
            ((Panel)(Application["room"])).Controls.Add(label);//发送
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Page_Load(sender, e);//刷新
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("webFrom.aspx");//退出
            Session.Abandon();
        }
    }
}