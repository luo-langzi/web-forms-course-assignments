﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="WebApplication1.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Panel ID="Panel1" runat="server"></asp:Panel>
            <asp:Panel ID="Panel2" runat="server">
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            </asp:Panel>
            <asp:Button ID="Button1" runat="server" Text="发送" OnClick="Button1_Click" />
            <asp:Button ID="Button2" runat="server" Text="刷新" OnClick="Button2_Click" />
            <asp:Button ID="Button3" runat="server" Text="退出" OnClick="Button3_Click" />
        </div>
    </form>
</body>
</html>
