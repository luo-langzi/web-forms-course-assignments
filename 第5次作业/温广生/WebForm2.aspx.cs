﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["name"] != null)
            {
                if (Application["speak"] == null)
                {
                    Application["speak"] = new Panel();
                }
                this.Panel1.Controls.Add((Panel)Application["speak"]);
            }
            else {
                Response.Redirect("webform1.aspx");
            }
        }
        


        protected void Button1_Click(object sender, EventArgs e)
        {
            string a = TextBox1.Text;
            string name = Session["name"].ToString();
            string b = DateTime.Now.ToString();
            Literal li = new Literal();
            li.Text = name + " " + b + ":" + a + "< br />";
            ((Panel)Application["speak"]).Controls.Add(li);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Page_Load(sender,e);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("WebForm1.aspx");
        }
    }
}