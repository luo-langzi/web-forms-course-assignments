﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Demo1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
<%--            编写一个注册页面，需要有如下验证功能：

   姓名：必填，且不能为admin

   密码：必填

   再次输入密码：必填，而且必须一致

   年龄：0~100

   出生日期：格式正确，范围1900-01-01到系统当前时间

   毕业时间：格式正确，不能早于出生日期

   邮箱：格式正确

   提交

   错误汇总：单段，弹框。

   注册成功！ 超链接：去登陆



登录页面！--%>
            姓名:<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="主人,您的用户名不能为空噢!" ControlToValidate="TextBox1" Font-Size="Larger" ForeColor="Maroon"></asp:RequiredFieldValidator><asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="嘿嘿,用户名也不能为admin!" ControlToValidate="TextBox1" OnServerValidate="CustomValidator1_ServerValidate" ForeColor="Maroon"></asp:CustomValidator>
            <br />
            密码:<asp:TextBox ID="TextBox2" runat="server" TextMode ="Password"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="密码不能为空噢,没有密码的账号是没有灵魂滴!" ForeColor="Maroon" ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
            <br />
            确认密码:<asp:TextBox ID="TextBox3" runat="server" TextMode ="Password"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="密码不能为空噢!" ForeColor="#993333" ControlToValidate="TextBox3"></asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="主人,不要着急嘛,您太快了,密码都不一致了!" ControlToCompare="TextBox2" ControlToValidate="TextBox3"></asp:CompareValidator>
            <br />
            年龄:<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="主人,您是长生不老,还是还在娘胎里呀,年龄要在0~120之间噢!" ControlToValidate="TextBox4" MaximumValue="120" MinimumValue="0" Type="Integer"></asp:RangeValidator>
            <br />
            出生日期:<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="出生日期要在2000-01-01至当前时间" MinimumValue="1900-01-01" ControlToValidate="TextBox5"></asp:RangeValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="时间格式必须为2000-01-01" ControlToValidate="TextBox5" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
            <br />
            毕业时间:<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="主人,您是还没出生就毕业了啊,范围请在出生时间之后!" ControlToValidate="TextBox6" ControlToCompare="TextBox5" Operator="GreaterThan"></asp:CompareValidator>
            <br />
            邮箱:<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="主人,您该不会连邮箱都没有吧,连小奴我都有邮箱嘞!真瞧不起主人,嘿嘿!" ControlToValidate="TextBox7"></asp:RequiredFieldValidator>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            <br />
            
        </div>
    </form>
</body>
</html>
