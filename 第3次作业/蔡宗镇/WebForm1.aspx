﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

               ————注册页面————
            <br />
             用户名：</asp:Label>   <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="请输入用户名！" ControlToValidate="TextBox1"></asp:RequiredFieldValidator><asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="且不能为admin" OnServerValidate="CustomValidator1_ServerValidate" ControlToValidate="TextBox1" SetFocusOnError="True"></asp:CustomValidator>
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="请输入密码！" ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
            <br />
            确认密码：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox> <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="请输入密码！" ControlToValidate="TextBox3"></asp:RequiredFieldValidator>     <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="密码必须一致！" ControlToCompare="TextBox2" ControlToValidate="TextBox3"></asp:CompareValidator>
             <br />
             年龄：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="年纪0-99" MaximumValue="99" MinimumValue="0" ControlToValidate="TextBox4"></asp:RangeValidator>
            <br />
            出生日期：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="1900-01-01到系统当前时间" MinimumValue="1900-01-01" ControlToValidate="TextBox5"></asp:RangeValidator>
             <br />
             毕业日期：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator3" runat="server" ErrorMessage="1900-01-01不能早于系统当前时间" MinimumValue="1900-01-01" ControlToValidate="TextBox6"></asp:RangeValidator>
            <br />
            邮箱：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="请输入格式正确" ControlToValidate="TextBox7" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <br />
            个人头像;<asp:FileUpload ID="FileUpload1" runat="server" /><br />
           
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" /><br />
            <asp:Label ID="Label2" runat="server" Text="登录"></asp:Label>
             <asp:Image ID="Image1" runat="server" /><asp:Label ID="Label3" runat="server" Text="Label"></asp:Label><br />
        </div>
    </form>
</body>
</html>
