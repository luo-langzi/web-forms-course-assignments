﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 注册页面</b>
            <br />
            用户名：&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="请填写用户名！" ControlToValidate="TextBox1" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="用户名不能为admin!" ControlToValidate="TextBox1" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
            <br />

            密码：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox2" runat="server" TextMode ="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="请填写密码！" ControlToValidate="TextBox2" Display="Dynamic"></asp:RequiredFieldValidator>
            <br />

            确认密码：<asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次密码不一致！" ClientIDMode="Inherit" ControlToCompare="TextBox2" ControlToValidate="TextBox3" Display="Dynamic"></asp:CompareValidator>
            <br />

            年龄：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="年龄应在1—50之间！" ControlToValidate="TextBox4" MaximumValue="50" MinimumValue="1" Display="Dynamic"></asp:RangeValidator>
            <br />

            出生日期：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="格式不正确！" ControlToValidate="TextBox5" ValidationExpression="\d{4}-\d{2}-\d{2}" Display="Dynamic"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="1900-01-01到今天" ControlToValidate="TextBox5" Display="Dynamic" MinimumValue="1900-01-01" OnDataBinding="Page_Load"></asp:RangeValidator>
            <br />

            毕业时间：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="格式不正确！" ControlToValidate="TextBox6" ValidationExpression="\d{4}-\d{2}-\d{2}" Display="Dynamic"></asp:RegularExpressionValidator>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="不得低于出生日期！" ControlToCompare="TextBox5" ControlToValidate="TextBox6" Operator="GreaterThan"></asp:CompareValidator>
            <br />

            邮箱：&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox7" runat="server" TextMode="Email"> </asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="邮箱格式不正确！" ControlToValidate="TextBox7" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <br />

            上传头像：<asp:FileUpload ID="ful" runat="server" /><%--<asp:Button ID="Button2" runat="server" Text="上传" OnClick="Button2_Click" />--%>

            <br />

            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            <br />
            <a href = '/WebForm2.aspx'> 去登录 </a>
            
        </div>
    </form>
</body>
</html>
