﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication2.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            姓名:<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="姓名不能是admin" ControlToValidate="TextBox1" Display="Dynamic" ValidationGroup="register"></asp:CustomValidator><asp:CompareValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="用户名不能为空！" ControlToCompare="TextBox1" ></asp:CompareValidator>
            <br/>
            密码:<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="密码不能为空" ControlToValidate="TextBox2" ValidationGroup="register"></asp:RequiredFieldValidator>
            <br/>
            确认密码:<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="请在次输入密码" ControlToValidate="TextBox3" ValidationGroup="register"></asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="两次密码不一致" ControlToCompare="TextBox2" ControlToValidate="TextBox3"></asp:CompareValidator>
            <br/>
            年龄:<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="年龄在0~100之间" MaximumValue="100" MinimumValue="0" Type="Integer"></asp:RangeValidator>
            <br/>
            出生日期:<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="格式必须为2020-12-13" ControlToValidate="Text5" Display="Dynamic" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator><asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="出生日期必须在2000-01-01到当前时间之间！" ControlToValidate="TextBox5" MinimumValue="1990-01-01"></asp:RangeValidator>
            <br/>
            毕业时间:<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="不能早于出生日期" ControlToCompare="Text5" ControlToValidate="Text6" Operator="GreaterThan" Type="Date"></asp:CompareValidator>
            <br/>
            邮箱:<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
            <br/>
            <asp:Button ID="Button1" runat="server" Text="提交" />
        </div>
    </form>
</body>
</html>
