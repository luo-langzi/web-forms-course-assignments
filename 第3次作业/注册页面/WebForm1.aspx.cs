﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RangeValidator1.MaximumValue = DateTime.Now.ToLongDateString();
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value == "admin")
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
            if (FileUpload1.HasFile)//判断有没有文件
            {
                string filename = FileUpload1.FileName;
                if (filename.EndsWith(".jpg") || filename.EndsWith(".jpeg") || filename.EndsWith(".png") || filename.EndsWith(".gif"))
                {
                    string url = Server.MapPath(@"~\img\") + filename;
                    FileUpload1.SaveAs(url);
                    Image1.ImageUrl = @"~\img\" + filename;
                    Image1.Visible = true;
                }
                else
                {
                    Response.Write("<script>alert('aaa格式不对，只能jpg,jpeg,png,gif！')</script>");
                    this.ClientScript.RegisterClientScriptBlock(GetType(), "提示", "<script>alert('11格式不对，只能jpg,jpeg,png,gif！！')</script>");
                    Literal1.Text = "<script>alert('格式不对，只能jpg,jpeg,png,gif！')</script>";
                }
            }
            else
            {
                Response.Write("<script>alert('bbb格式不对，只能jpg,jpeg,png,gif！')</script>");
                this.ClientScript.RegisterClientScriptBlock(GetType(), "提示", "<script>alert('11不能上传空文件！')</script>");
                Literal1.Text = "<script>alert('不能上传空文件！')</script>";
    }
}

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (IsValid == true)
            {
                Literal1.Text = "注册成功！<a href = '/WebForm3.aspx'>点击跳转登陆页面</a>";
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

        }
    }
}