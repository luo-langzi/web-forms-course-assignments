﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <%--# 编写一个注册页面，需要有如下验证功能：
        姓名：必填，且不能为admin
        密码：必填
        再次输入密码：必填，而且必须一致
        年龄：0~100
        出生日期：格式正确，范围1900-01-01到系统当前时间
        毕业时间：格式正确，不能早于出生日期
        邮箱：格式正确
        提交按钮
        错误汇总：单段，弹框。
        所有验证通过的话，按钮下面提示语：注册成功！ 超链接：去登陆
        跳转到登陆页面，登录页面展示：登录页面！--%>

            姓名：
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="姓名不能为空！" ControlToValidate="TextBox1" Font-Bold="True" Display ="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="姓名不可为admin" ControlToValidate="TextBox1" Display ="Dynamic" ValidationGroup="register" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
            <br />
            密码：
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="密码不能为空！" ControlToValidate ="TextBox2" Display ="Dynamic" ValidationGroup ="register"></asp:RequiredFieldValidator>
            <br />
            再次输入密码：
            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="请再次输入密码" ControlToValidate ="TextBox3" ValidationGroup="register" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次输入的不一样" ControlToCompare ="TextBox2" ControlToValidate ="TextBox3" ValidationGroup ="register"></asp:CompareValidator>
            <br />
            年龄：
            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="请输入的正确年龄！" ControlToValidate ="TextBox4" MaximumValue="100" MinimumValue="0" Type ="String"></asp:RangeValidator>
            <br />
            出生日期：
            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="格式有误" ControlToValidate ="TextBox5" ValidationExpression ="\d{4}-\d{2}-\d{2}" Display ="Dynamic"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="出生日期必须在2021-06-11到当前时间之间！" ControlToValidate ="TextBox5" MaximumValue ="2021-12-31" MinimumValue ="1990-01-01"></asp:RangeValidator>
            <br />
            毕业时间：
            <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="毕业时间要大于出生时间" ControlToCompare ="TextBox5" ControlToValidate ="TextBox6" Operator ="GreaterThan" Type="Date" ></asp:CompareValidator>
            <br />
            邮箱：
            <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="格式有误" ControlToValidate="TextBox7" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <br />
            头像：
            <asp:FileUpload ID="FileUpload1" runat="server" />
            <asp:Button ID="Button2" runat="server" Text="上传" OnClick ="Button2_Click"/>
            <asp:Image ID="Image1" runat="server" />
            <asp:Literal ID="Literal2" runat="server"></asp:Literal>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick ="Button1_Click" ValidationGroup ="register"/>
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="SingleParagraph" ShowMessageBox ="true" />

        </div>
    </form>
</body>
</html>
