﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            用户名：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="用户名不能为空！" ControlToValidate="TextBox1" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="用户名不能为admin！" Display="Dynamic" ControlToValidate="TextBox1" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
           
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="密码不能为空！" ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
           
            <br />
            确认密码：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="密码不能为空!" ControlToValidate="TextBox3"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次密码不一致" ControlToCompare="TextBox2" ControlToValidate="TextBox3"></asp:CompareValidator>
        
            <br />
            年龄：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="必须在0~100" ControlToValidate="TextBox4" MinimumValue="0" MaximumValue="100" Type="Integer"></asp:RangeValidator>

            <br />
            出生日期：<asp:TextBox ID="TextBox5" runat="server" OnTextChanged="TextBox5_TextChanged"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="格式为1900-01-01" ValidationExpression="\d{4}-\d{2}-\d{2}" ControlToValidate="TextBox5" Display="Dynamic"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="范围1900-01-01到当前时间" ControlToValidate="TextBox5" Display="Dynamic" MinimumValue="1900-01-01"></asp:RangeValidator>

            <br />
            毕业时间：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="格式为1900-01-01" ValidationExpression="\d{4}-\d{2}-\d{2}" ControlToValidate="TextBox5"></asp:RegularExpressionValidator>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="不能早于出生日期" ControlToValidate="TextBox6" Display="Dynamic" ControlToCompare="TextBox5" Operator="GreaterThan"></asp:CompareValidator>
           
            <br />
            邮箱：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="邮箱不为空" ControlToValidate="TextBox7"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="格式不对" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="TextBox7"></asp:RegularExpressionValidator>
            
            <br />
            <asp:Image ID="Image1" runat="server" />
            <asp:FileUpload ID="FileUpload1" runat="server" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="图片不能为空" ControlToValidate="FileUpload1" Display="Dynamic"></asp:RequiredFieldValidator>
           
            
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />

            <br />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />

            <br />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
