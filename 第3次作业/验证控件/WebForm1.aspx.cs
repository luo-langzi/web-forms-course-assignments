﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RangeValidator2.MaximumValue = DateTime.Now.ToShortDateString();

            if (FileUpload1.HasFile)
            {
                string filename = FileUpload1.FileName;
                if (filename.EndsWith("jpg")| filename.EndsWith("jpeg") |filename.EndsWith("gif"))
                {
                    string uil = Server.MapPath("/img/"+filename);
                    FileUpload1.SaveAs(uil);
                    Image1.Visible = true;
                    Image1.ImageUrl = "/img/" + filename;
                }
            }
            else
            {
                Response.Write("头像不为空");
                return;
            }
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if(args.IsValid.Equals("admin"))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void TextBox5_TextChanged(object sender, EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if(IsValid == true)
            {
                Literal1.Text = "注册成功！<a href = '/WebForm2.aspx'>去注册</a>";
            }
            else
            {

            }
        }
    }
}