﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RangeValidator2.MaximumValue = DateTime.Now.ToString();
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (TextBox1.Text=="admin")
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void TextBox5_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (IsValid == true)
            {
             
                Literal1.Text = "注册成功！<a href='/WebForm2.aspx'>点击跳转登录页面</a>";
            }
            else
            {
                Literal1.Text = "注册失败";
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string fileName = this.FileUpload1.FileName;
            if (this.FileUpload1.HasFile)
            {
                if (fileName.EndsWith(".jpg") || fileName.EndsWith(".png") || fileName.EndsWith(".jpeg"))
                {
                    string strPath = Server.MapPath(@"~\img\" + fileName);
                    this.FileUpload1.SaveAs(strPath);
                    this.Image1.ImageUrl = "~/img/" + fileName;
                }

            }
        }
    }
}