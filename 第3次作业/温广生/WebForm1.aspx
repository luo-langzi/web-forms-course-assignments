﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication3.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             账号：<asp:TextBox ID="TextBox1" runat="server" ValidationGroup="re"></asp:TextBox><asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="用户不能为admin" Display="Dynamic" ControlToValidate="TextBox1" ValidationGroup="re" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="用户名不能为空" ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
        <br />
            密码：<asp:TextBox ID="TextBox2" runat="server" ValidationGroup="re"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="密码不能为空" ControlToValidate="TextBox2"></asp:RequiredFieldValidator>

        <br />
             确认密码：<asp:TextBox ID="TextBox3" runat="server" ValidationGroup="re"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="请再次输入密码" ControlToValidate="TextBox3" Display="Dynamic"></asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次密码不一样" ControlToCompare="TextBox2" ControlToValidate="TextBox3" Display="Dynamic"></asp:CompareValidator>
    <br />
        年龄：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="年龄必须在100以内" MaximumValue="100"  Display="Dynamic" Type="Integer" ControlToValidate="TextBox4" MinimumValue="0"></asp:RangeValidator>
        <br />
        出生日期：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="格式必须为：2000-01-01" ControlToValidate="TextBox5" ValidationExpression="\d{4}-\d{2}-\d{2}" Display="Dynamic"></asp:RegularExpressionValidator><asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="出生日期必须在1990-01-01到至今" ControlToValidate="TextBox5" Display="Dynamic" MinimumValue="1990-01-01"></asp:RangeValidator>
        <br />
        毕业日期：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="必须要大于2000-01-01" ControlToCompare="TextBox5" ControlToValidate="TextBox6" Display="Dynamic" Operator="GreaterThan" Type="Date"></asp:CompareValidator>
        <br />
        邮箱：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox><asp:RegularExpressionValidator runat="server" ErrorMessage="邮箱的格式为abc@abc.qq.com||\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" ControlToValidate="TextBox7" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        <br />
        <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click"  />
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
