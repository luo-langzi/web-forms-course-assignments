﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)  //判断是否有或取文件
            {
                string imgname = FileUpload1.FileName;    //有取到文件将他的名字赋给字符串   
                if (imgname.EndsWith(".jpg")|| imgname.EndsWith(".jpeg") || imgname.EndsWith(".gif") || imgname.EndsWith(".png") )
                {
                    string url = Server.MapPath(@"~\img\") + imgname; //获取图片路径 
                    FileUpload1.SaveAs(url);                          //将图片上传到web服务器的指定位置
                    Image1.ImageUrl = @"~\img\" + imgname;            //图片控件要显示的图片的位置
                    Image1.Visible = true;
                }
                else
                {
                    Literal1.Text = "<script>alert('图片格式不对 重新上传')</script>";
                }
            }
            else
            {
                Literal1.Text = "<script>alert('不能上传空文件')</script>";    //<script>alert</script>脚本标签  弹出警告                                                               //<script>alert('')</script> 格式
            }
        }
    }
}