﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication2.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            姓 名:
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="此项不能为空" ControlToValidate="TextBox1" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="用户名不能为admin" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
            <br />
            密 码：
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="此项不能为空" ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
            <br />
            确认密码：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBox2" ControlToValidate="TextBox7" ErrorMessage="两次密码不一致" Display="Dynamic" Type="Integer"></asp:CompareValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="不能为空" ControlToValidate="TextBox7" Display="Dynamic"></asp:RequiredFieldValidator>
            <br />
            年龄：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
&nbsp;<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TextBox3" ErrorMessage="填写0-100范围内的值" MaximumValue="100" MinimumValue="0" Type="Integer"></asp:RangeValidator>
            <br />
            出生日期：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="请输入正确格式的日期" ControlToValidate="TextBox4" ValidationExpression="(\d{4})-(\d{2})-(\d{2})"></asp:RegularExpressionValidator>
            <br />
            毕业日期：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="请输入正确格式的日期" ControlToValidate="TextBox5" Display="Dynamic" ValidationExpression="(\d{4})-(\d{2})-(\d{2})"></asp:RegularExpressionValidator>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="不能早于毕业日期" ControlToCompare="TextBox4" ControlToValidate="TextBox5" Display="Dynamic" Operator="GreaterThan" Type="Date"></asp:CompareValidator>
            <br />
            邮 箱：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="请输入正确格式的邮箱" ControlToValidate="TextBox6" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            <br />
        </div>
    </form>
</body>
</html>
