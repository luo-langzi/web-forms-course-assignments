﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value == "admin")   
            {
                args.IsValid = false;   //验证不通过 就是用户名出错
            }
            else
            {
                args.IsValid = true;
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (IsValid==true)
            {
                Literal1.Text = "注册成功<a href='/WebForm2.aspx'>去登陆</a>"; // 用标签写超链接
            }
        }
    }
}