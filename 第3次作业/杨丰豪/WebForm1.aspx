﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication10.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
           账&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 号：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage=" 账号必填！" ControlToValidate="TextBox1" Font-Bold="True" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="不能为admin!" ControlToValidate="TextBox1" Font-Bold="true" Display="Dynamic" OnServerValidate="CustomValidator1_ServerValidate" ForeColor="Red"></asp:CustomValidator>
            <br />
           密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 码：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="密码必填！" Font-Bold="true" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
            <br />
           确认密码：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="请再次输入密码!" Font-Bold="true" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox3"></asp:RequiredFieldValidator>
           <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次密码不一致！" ControlToCompare="TextBox2" ControlToValidate="TextBox3" Font-Bold="true" ForeColor="Red" ></asp:CompareValidator>
            <br />
            年&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 龄：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="年龄范围0—120岁之间!" ControlToValidate="TextBox4" Font-Bold="true" ForeColor="Red" MaximumValue="120" MinimumValue="0"></asp:RangeValidator>
        <br />
            出身日期：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="格式如:2000-01-01" ControlToValidate="TextBox5" Display="Dynamic" ValidationExpression="\d{4}-\d{2}-\d{2}" ForeColor="Red" Font-Bold="true"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="出生日期必须在2000-01-01到当前时间之间！" Font-Bold="True" ForeColor="Red" Display="Dynamic" MinimumValue="2000-01-01" ControlToValidate="TextBox5"></asp:RangeValidator>
        <br />
            毕业时间：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="毕业日期不能早于出生日期！" ControlToCompare="TextBox5" ControlToValidate="TextBox6" Operator="GreaterThan" Font-Bold="True" ForeColor="Red"></asp:CompareValidator>
        <br />
            邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 箱：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="邮箱格式不正确!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Font-Bold="True" Font-Italic="False" ForeColor="Red" ControlToValidate="TextBox7"></asp:RegularExpressionValidator>
        <br />
            头&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 像：<asp:FileUpload ID="FileUpload1" runat="server"/><asp:Button ID="Button2" runat="server" Text="上传" OnClick="Button2_Click"/>
            <asp:Image ID="Image1" runat="server" /><asp:Literal ID="Literal1" runat="server"></asp:Literal>
            <br />
        <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button1" runat="server" Text="提交" Width="78px" OnClick="Button1_Click" />
            <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
