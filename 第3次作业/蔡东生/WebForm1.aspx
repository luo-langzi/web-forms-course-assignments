﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>
<script runat="server">

    protected void Button2_Click(object sender, EventArgs e)
    {

    }
</script>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            用户名：<asp:TextBox ID="TextBox1" runat="server" ></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" Visible="True" ControlToValidate="TextBox1">必填，且不能为admin</asp:RequiredFieldValidator>
            <br />
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server" ></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator" Font-Bold="False" Text="必填" ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
            <br />
            <br />
            再次输入密码：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox><asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="CompareValidator" ControlToCompare="TextBox2" ControlToValidate="TextBox3">必填，而且必须一致</asp:CompareValidator>
            <br />
            <br />
            年龄：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="RangeValidator" ControlToValidate="TextBox4" MaximumValue="100" MinimumValue="0">0~100</asp:RangeValidator>
            <br />
            <br />
            出生日期：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" Display="Dynamic" ValidationExpression="\d{4}-\d{2}-\d{2}" ControlToValidate="TextBox5">格式正确，范围1900-01-01到系统当前时间</asp:RegularExpressionValidator><asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="格式正确，范围1900-01-01到系统当前时间" ControlToValidate="TextBox5" MinimumValue="1990-01-01"></asp:RangeValidator>
            <br />
            <br />
            毕业日期：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="CompareValidator" ControlToCompare="TextBox5" ControlToValidate="TextBox6" Operator="GreaterThan" Type="Date">格式正确，不能早于出生日期</asp:CompareValidator>
            <br />
            <br />
            邮箱：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="RegularExpressionValidator" ControlToValidate="TextBox7" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">格式正确</asp:RegularExpressionValidator>
            <br />
            <br />
            <asp:FileUpload ID="FileUpload1" runat="server" />
            <br />
            <br />
            <asp:Button ID="Button2" runat="server" Text="上传" OnClick="Button2_Click" />
            <asp:Image ID="Image1" runat="server" />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
