﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication7
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RangeValidator2.MaximumValue = DateTime.Now.ToShortDateString();
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value == "admin")
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (IsValid == true)
            {
                Literal1.Text = "注册成功！<a href='/WebForm2.aspx'>去登录</a>";
            }
            else
            {
                Literal1.Text = "注册失败,请重新输入！";
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                string mane = FileUpload1.FileName;
                if (mane.EndsWith(".jpg") || mane.EndsWith(".jpeg") || mane.EndsWith(".png") || mane.EndsWith(".gif"))
                {
                    string u = Server.MapPath(@"~\img\") + mane;
                    FileUpload1.SaveAs(u);
                    Image1.ImageUrl = @"~\img\" + mane;
                    Image1.Visible = true;
                    Literal2.Text = "头像上传成功！";
                }
                else
                {
                    this.ClientScript.RegisterClientScriptBlock(GetType(), "提示", "<script>alert('上传图片格式有误，只能为jpg,jpeg,png,gif！')</script>");
                }
            }
        }
    }
    }
