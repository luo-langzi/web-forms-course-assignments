﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication7.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="欢迎来到注册页面！" Font-Bold="True" Font-Size="Larger" ForeColor="#FF5050"></asp:Label>
            <br />
            头像：<asp:FileUpload ID="FileUpload1" runat="server" /><asp:Button ID="Button2" runat="server" Text="上传" OnClick="Button2_Click" /><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="头像不能为空！" ValidationGroup="register" Display="Dynamic" ControlToValidate="FileUpload1"></asp:RequiredFieldValidator>
            <br />
            <asp:Image ID="Image1" runat="server" />
            <asp:Literal ID="Literal2" runat="server"></asp:Literal>
            <br />
            姓名：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="姓名不能为admin!" ControlToValidate="TextBox1" Display="Dynamic" OnServerValidate="CustomValidator1_ServerValidate" ValidationGroup="register"></asp:CustomValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="姓名不能为空！" ControlToValidate="TextBox1" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="密码不能为空！" ControlToValidate="TextBox2" ValidationGroup="register" Display="Dynamic"></asp:RequiredFieldValidator>
            <br />
            再次输入密码：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="再次输入密码不能为空！" ControlToValidate="TextBox3" ValidationGroup="register" Display="Dynamic"></asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次密码不一致！" ControlToCompare="TextBox2" ControlToValidate="TextBox3" ValidationGroup="register" Display="Dynamic"></asp:CompareValidator>
            <br />
            年龄：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="年龄必须在0~100之间！" ControlToValidate="TextBox4" MaximumValue="100" MinimumValue="0" Type="Integer" ValidationGroup="register" Display="Dynamic"></asp:RangeValidator>
            <br />
            出生日期：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="格式必须为：1900-01-01!" ControlToValidate="TextBox5" Display="Dynamic" ValidationGroup="register" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator><asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="出生日期必须在1900-01-01 到 当前时间之间！" ControlToValidate="TextBox5" MinimumValue="1900-01-01" ValidationGroup="register" Display="Dynamic" ></asp:RangeValidator>
            <br />
            毕业时间：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="毕业时间不能早于出生日期！" ControlToCompare="TextBox5" ControlToValidate="TextBox6" Operator="GreaterThan" Type="Date" ValidationGroup="register" Display="Dynamic"></asp:CompareValidator>       
            <br />
            邮箱：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="格式必须为：11111@11111.com" ControlToValidate="TextBox7" Display="Dynamic" ValidationExpression="\d{5}@\d{5}.com" ValidationGroup="register"></asp:RegularExpressionValidator>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" ValidationGroup="register" />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            <br />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="SingleParagraph" ShowMessageBox="True" />
        </div>
    </form>
</body>
</html>
