﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RangeValidator2.MaximumValue = DateTime.Now.ToShortDateString();

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (IsValid == false)
            {
                Response.Write($"<scirpt>alert({ValidationSummary1})</scirpt>");
            }
            else
            {
                Response.Write("注册成功");
            }
            if (FileUpload1.HasFile)
            {
                string a = FileUpload1.FileName;
                if (a.EndsWith(".img")||a.EndsWith(".png")||a.EndsWith(".gif"))
                {
                    string url = Server.MapPath(@"~\img\") + a;
                    FileUpload1.SaveAs(url);
                    Image1.ImageUrl= (@"~\img\") + a;
                    Image1.Visible = true;
                }
                else
                {
                    RequiredFieldValidator4.Text = "不支持该文件格式";
                }
            }
           
        }



        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value=="admin")
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
            
        }
    }
}