﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            头像：<asp:Image ID="Image1" runat="server" />
            <asp:FileUpload ID="FileUpload1" runat="server" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="头像不能为空" ControlToValidate="FileUpload1"></asp:RequiredFieldValidator>
            <br />
            用户名:<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="用户名不能为admin" ControlToValidate="TextBox1" Display="Dynamic" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="用户名不能为空" ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="密码不能为空" ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
            <br />
            确认密码：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox><asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次输入不一致" ControlToCompare="TextBox2" ControlToValidate="TextBox3" Display="Dynamic"></asp:CompareValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="请再次输入密码" ControlToValidate="TextBox3"></asp:RequiredFieldValidator>
            <br />
            年龄：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="年龄不能超出0-100" ControlToValidate="TextBox6" MaximumValue="100" MinimumValue="0" Type="Integer"></asp:RangeValidator>
            <br />
            出生日期：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="格式必须为XXXX-XX-XX" ControlToValidate="TextBox5" Display="Dynamic" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="TextBox5" ErrorMessage="范围必须为1900-01-01到系统当前时间" MinimumValue="1900-01-01" Type="Date"></asp:RangeValidator>
            <br />
            毕业日期：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox6" ErrorMessage="格式必须为XXXX-XX-XX" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="TextBox4" ControlToValidate="TextBox5" ErrorMessage="必须大于出生日期" Operator="GreaterThan" Type="Date"></asp:CompareValidator>
            <br />
            邮箱：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBox7" ErrorMessage="格式不正确" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" OnClient="Buttonl Client"/>
            &nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/WebForm2.aspx">去登陆</asp:HyperLink>
            <br />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="SingleParagraph" ShowMessageBox="True" />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>

        </div>
    </form>
</body>
</html>
