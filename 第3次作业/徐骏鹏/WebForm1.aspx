﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="好好学习.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server" method="post">
        <div>
            <table>
            <tr>
            <td><asp:Label ID="Label1" runat="server" Text="姓名："></asp:Label></td>
               <td> <asp:TextBox ID="TextBox1" runat="server" ></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="姓名不能为空" ControlToValidate="TextBox1"></asp:RequiredFieldValidator></td>
                <td> <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="账号不能为admin！" ControlToValidate="TextBox1" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator></td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label2" runat="server" Text="密码："></asp:Label></td>
                <td><asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="密码不能为空" ControlToValidate="TextBox2"></asp:RequiredFieldValidator></td>
            </tr>
             <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text=" 请再次输入密码："></asp:Label></td>
                <td><asp:TextBox ID="TextBox3" runat="server"></asp:TextBox><asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="前后输入的密码不同"  ControlToCompare="TextBox3" ControlToValidate="TextBox2"></asp:CompareValidator></td>
            </tr>
             <tr>
                <td><asp:Label ID="Label4" runat="server" Text="年龄："></asp:Label></td>
                <td><asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="输入的年龄有问题" ControlToValidate="TextBox4" MaximumValue="100" MinimumValue="0"></asp:RangeValidator></td>
            </tr>
             <tr>
                <td> <asp:Label ID="Label5" runat="server" Text="出生日期："></asp:Label></td>
                <td><asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="你的出生日期有误" ControlToValidate="TextBox5" MaximumValue="2021-07-01" MinimumValue="1900-01-01" Type="Date"></asp:RangeValidator></td>
            </tr>
             <tr>
                <td> <asp:Label ID="Label6" runat="server" Text="邮箱："></asp:Label> </td>
                <td><asp:TextBox ID="TextBox6" runat="server"></asp:TextBox> <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="请输入正确的邮箱账号" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="TextBox6"></asp:RegularExpressionValidator></td>
            </tr>
            <tr>
                    <td><asp:ValidationSummary ID="ValidationSummary1" runat="server"   /></td>
            </tr>
             <tr>
                <td> <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" PostBackUrl="~/登录.aspx"/></td>
                
           </tr>
                
       </table>
        </div>
    </form>
</body>
</html>
