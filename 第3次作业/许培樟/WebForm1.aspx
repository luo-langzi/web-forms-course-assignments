﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication2.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    
    <form id="form1" runat="server">
        <div>
            注册页面
            用户名：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="用户名不能为admin" Display="Dynamic" OnServerValidate="CustomValidator1_ServerValidate" ControlToValidate="TextBox1"></asp:CustomValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="用户名不能为空！" ControlToValidate="TextBox1" Display="Dynamic"></asp:RequiredFieldValidator>
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="密码不能为空！" ControlToValidate="TextBox2" Display="Dynamic"></asp:RequiredFieldValidator>
            <br />
            确认密码：<asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="确认密码不能为空" Display="Dynamic" ControlToValidate="TextBox3"></asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次密码不一致" ControlToCompare="TextBox2" ControlToValidate="TextBox3" Display="Dynamic"></asp:CompareValidator>
            <br />
            年龄：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="年龄在0~100之间" ControlToValidate="TextBox4" MaximumValue="100" MinimumValue="0" Display="Dynamic" Type="Integer"></asp:RangeValidator>
            <br />
            出生日期：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="日期格式为：2001-01-01" ControlToValidate="TextBox5" Display="Dynamic" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator><asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="出生日期为1900-01-01到当前时间" ControlToValidate="TextBox5" Display="Dynamic" MinimumValue="1900-01-01"></asp:RangeValidator>
            <br />
            毕业日期：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="毕业日期不能小于出生日期" ControlToCompare="TextBox5" ControlToValidate="TextBox6" Display="Dynamic" Operator="GreaterThan"></asp:CompareValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="格式为：2000-01-01" ControlToValidate="TextBox6" Display="Dynamic" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
            <br />
            邮箱：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="邮箱格式错误" ControlToValidate="TextBox7" Display="Dynamic" ValidationExpression="\w{10}@\w{2}.((com)||(cn))"></asp:RegularExpressionValidator>
            <br />
            头像：<asp:FileUpload ID="FileUpload1" runat="server" /><asp:Button ID="Button2" runat="server" Text="上传" OnClick="Button2_Click" /><asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="CustomValidator" Display="Dynamic"></asp:CustomValidator>
            <br />
            <asp:Image ID="Image1" runat="server" />
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            <br />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List" ShowMessageBox="True" />
        </div>
    </form>
</body>
</html>
