﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="注册.aspx.cs" Inherits="WebApplication2.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
<%--# 编写一个注册页面，需要有如下验证功能：

   姓名：必填，且不能为admin

   密码：必填

   再次输入密码：必填，而且必须一致

   年龄：0~100

   出生日期：格式正确，范围1900-01-01到系统当前时间

   毕业时间：格式正确，不能早于出生日期

   邮箱：格式正确

   提交按钮

   错误汇总：单段，弹框。

   所有验证通过的话，按钮下面提示语：注册成功！ 超链接：去登陆

 跳转到登陆页面，登录页面展示：登录页面！--%>
            账户姓名：<asp:TextBox ID="TextBox1" runat="server" ControlToValidate="TextBox1"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="用户名不能为空！" ControlToValidate="TextBox1" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="用户名不能为admin" OnServerValidate="CustomValidator1_ServerValidate" Display="Dynamic" ControlToValidate="TextBox1"></asp:CustomValidator>
            <br/>
            账户密码：<asp:TextBox ID="TextBox2" runat="server" ControlToValidate="TextBox2"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="密码不能为空！" ControlToValidate="TextBox2" Display="Dynamic"></asp:RequiredFieldValidator>
            <br/>
            确定密码：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="密码不能为空！" ControlToValidate="TextBox3" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次输入密码不一致！" ControlToValidate="TextBox3" ControlToCompare="TextBox2" Display="Dynamic" Operator="Equal"></asp:CompareValidator>
            <br/>
            用户年龄：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="请输入年龄在0~100之间" ControlToValidate="TextBox4" MaximumValue="100" MinimumValue="0" Type="Integer" Display="Dynamic"></asp:RangeValidator>
            <br/>
            出生日期：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="请输入1900-01-01到系统当前时间范围" ControlToValidate="TextBox5" Display="Dynamic" MinimumValue="1900-01-01" Type="Date"></asp:RangeValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="日期格式必须为：如：1999-05-01" Display="Dynamic" ValidationExpression="\d{4}-\d{2}-\d{2}" ControlToValidate="TextBox5"></asp:RegularExpressionValidator>
            <br/>
            毕业日期：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="毕业日期不能早于出生日期！你是憨憨吗ε=(´ο｀*)))唉" Display="Dynamic" ControlToValidate="TextBox6" Operator="GreaterThan" ControlToCompare="TextBox5" Type="Date"></asp:CompareValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="日期格式必须为：如：1999-05-01" ControlToValidate="TextBox6" Display="Dynamic" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
            <br/>
            电子邮箱：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="邮箱格式必须为：如：1320548825@qq.com" ControlToValidate="TextBox7" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <br/>
            上传头像：<asp:FileUpload ID="FileUpload1" runat="server" /><asp:Button ID="Button2" runat="server" Text="上传" /><asp:Image ID="Image1" runat="server" /><asp:Literal ID="Literal2" runat="server"></asp:Literal>
            <br/>
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br/>
            错误汇总：<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" DisplayMode="SingleParagraph  " />
           <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>