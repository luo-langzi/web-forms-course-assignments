﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RangeValidator2.MaximumValue = DateTime.Now.ToShortDateString();
        }
        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value == "admin")
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (IsValid == true) 
            {
                Literal1.Text = " 注册成功！<a href='/登录.aspx'>去登陆</a>";
            }
            if (FileUpload1.HasFile)
            {
                string fulname = FileUpload1.FileName;
                if (fulname.EndsWith(".jpg") || fulname.EndsWith(".jpeg") || fulname.EndsWith(".png") || fulname.EndsWith(".gif"))
                {
                    string photo = Server.MapPath(@"~\Photo\") + fulname;
                    FileUpload1.SaveAs(photo);
                    Image1.ImageUrl = @"~\Photo\" + fulname;
                    Image1.Visible = true;
                }
                else
                {
                    this.ClientScript.RegisterClientScriptBlock(GetType(), "提示", "<script>alert('图片格式错误！只能上传jpg、jpeg、png、gif后缀名')</script>");/*弹窗*/
                    //Literal2.Text = "<script>alert('图片格式错误！只能上传jpg、jpeg、png、gif后缀名')</script>";
                    //Response.Write("<script>alert('图片格式错误！只能上传jpg、jpeg、png、gif后缀名')</script>");
                }
            }
            else
            {
                this.ClientScript.RegisterClientScriptBlock(GetType(), "提示", "<script>alert('不能上传空文件')</script>");/*弹窗*/
                //Literal2.Text = "<script>alert('不能上传空文件')</script>";
            }
            }
    }
}