﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication3.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>用户注册</h1>
            头像:<asp:FileUpload ID="FileUpload1" runat="server" />
            <br />
            <asp:Button ID="Button2" runat="server" Text="上传" OnClick="Button2_Click" />
            <br />
            <asp:Image ID="Image1" runat="server" />
            <br />
            <asp:Literal ID="Literal2" runat="server"></asp:Literal>
            <br />
            用户名：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="请输入用户名！" ControlToValidate="TextBox1"></asp:RequiredFieldValidator><asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="用户名不能为admin" ControlToValidate="TextBox1" OnServerValidate="CustomValidator1_ServerValidate" ></asp:CustomValidator>
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="请输入密码！" ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
            <br />
            确认密码：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="请确认密码！" ControlToValidate="TextBox3"></asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次密码不一致" ControlToCompare="TextBox2" ControlToValidate="TextBox3"></asp:CompareValidator>
            <br />
            年龄：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="年龄范围在0-100！" MaximumValue="100" MinimumValue="0" ControlToValidate="TextBox4" type="Integer"></asp:RangeValidator>
            <br />
            出生日期：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="格式应为xxxx-xx-xx" ValidationExpression="\d{4}-\d{2}-\d{2}" ControlToValidate="TextBox5"></asp:RegularExpressionValidator><asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="出生时间应在1900-01-01到当前时间" ControlToValidate="TextBox5" MinimumValue="1900-01-01"></asp:RangeValidator>
            <br />
            毕业日期：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="格式应为xxxx-xx-xx" ValidationExpression="\d{4}-\d{2}-\d{2}" ControlToValidate="TextBox6" ></asp:RegularExpressionValidator><asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="毕业日期不能小于于出生日期" ControlToCompare="TextBox5" ControlToValidate="TextBox6" Operator="GreaterThan" Type="Date"></asp:CompareValidator>
            <br />
            邮箱：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="格式错误" Display="Static" ControlToValidate="TextBox7" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="SingleParagraph" ShowMessageBox="True" />
        </div>
    </form>
</body>
</html>
