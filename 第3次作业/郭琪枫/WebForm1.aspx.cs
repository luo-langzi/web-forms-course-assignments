﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication3
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RangeValidator2.MaximumValue = DateTime.Now.ToShortDateString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (IsValid == true)
            {
                Literal1.Text = "注册成功！<a href='/WebForm2.aspx'>点击跳转登录页面</a>";
            }
            else
            {
                Literal1.Text = "<script>alert('')</script>";
            }
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value == "admin")
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                string filename = FileUpload1.FileName;
                if (filename.EndsWith(".jpg") || filename.EndsWith(".jpeg") || filename.EndsWith(".png") || filename.EndsWith(".gif"))
                {
                    string url = Server.MapPath(@"~\Img\") + filename;
                    FileUpload1.SaveAs(url);
                    Image1.ImageUrl = @"~\Img\" + filename;
                    Image1.Visible = true;
                    return;
                }
                else
                {
                    Literal2.Text = "<script>alert('格式不对，只能jpg,jpeg,png,gif！')</script>";
                    return;
                }
            }
            else
            {
                Literal2.Text = "<script>alert('不能上传空文件！')</script>";
                return;
            }
        }
    }
}