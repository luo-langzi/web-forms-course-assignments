﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RangeValidator2.MaximumValue = DateTime.Now.ToShortDateString();
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value == "admin")
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (IsValid == true)
            {
                Literal1.Text = "注册成功!<a href='/WebForm2.aspx'>点击跳转登录页面</a>";
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                string file = FileUpload1.FileName;
                if (file.EndsWith(".jpg") || file.EndsWith(".jpeg") || file.EndsWith(".png"))
                {
                    string url = Server.MapPath(@"~\img\") + file;
                    FileUpload1.SaveAs(url);
                    Image1.ImageUrl = @"~\img\" + file;
                    Image1.Visible = true;
                }
                else
                {
                    CustomValidator2.ErrorMessage = "格式错误（.jpg、.jpeg、.png）";
                    CustomValidator2.IsValid = false;
                }
            }
            else
            {
                CustomValidator2.ErrorMessage = "不能上传空文件";
                CustomValidator2.IsValid = false;
            }
        }
    }
}