﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication2.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            姓名：<asp:TextBox ID="TextBox1" runat="server" ></asp:TextBox>
            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="用户名不能是admin!"  ControlToValidate="TextBox1" Display="Dynamic" OnServerValidate ="CustomValidator1_ServerValidate" ValidationGroup ="register"></asp:CustomValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="用户名不能为空！" ControlToValidate="TextBox1" Font-Bold="true" Font-Size="Larger" ForeColor ="Red" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="密码不能为空！" ControlToValidate="TextBox2" Font-Bold="true" Font-Size="Larger" ForeColor ="Red" ValidationGroup ="register"></asp:RequiredFieldValidator>
            <br />
            再次输入密码：<asp:TextBox ID="TextBox3" runat="server" TextMode="Password" ></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="请再次输入正确的密码！" ControlToValidate ="TextBox3" ValidationGroup ="register"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次密码不一致！" ControlToCompare="TextBox2" ControlToValidate="TextBox3" ></asp:CompareValidator>
            <br />
            年龄：<asp:TextBox ID="TextBox4" runat="server" Width="20"></asp:TextBox>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="年龄必须在0-150之间！" ControlToValidate="TextBox4" MaximumValue ="150" MinimumValue="0" Type ="Integer"></asp:RangeValidator>
            <br />
            出生日期：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="格式必须为：2000-01-01" ControlToValidate="TextBox5" Display="Dynamic" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="出生日期必须在2000-01-01到当前时间之间！" ControlToValidate="TextBox5" MinimumValue ="1990-01-01"></asp:RangeValidator>
            <br />
            毕业时间：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="毕业日期不能早于出生日期！" ControlToCompare="TextBox5" ControlToValidate="TextBox6" Operator="GreaterThan" Type ="Date"></asp:CompareValidator>
            <br />
            邮箱：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" ValidationGroup="register" />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            <br />
            错误汇总：
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="SingleParagraph" ShowMessageBox="true" />
        </div>
    </form>
</body>
</html>
