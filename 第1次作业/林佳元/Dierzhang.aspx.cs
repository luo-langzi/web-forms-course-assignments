﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string now = DateTime.Now.ToString();
                Label5.Text = $"您好，当前时间为：{now}";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            
            Label6.Text = $"请确认您填写的信息：\r\n" +
                $"姓名：{TextBox1.Text}\r\n" +
                $"年龄：{TextBox2.Text}\r\n" +
                $"爱好：{TextBox3.Text}\r\n";
        }
    }
}