﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label6.Text = System.DateTime.Now.ToShortDateString();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            TextBox6.Text = $"请确认你填写的信息"+
             $"姓名：{TextBox2.Text} "+
            $"年龄：{TextBox3.Text} "+
              $"爱好:{TextBox4.Text}";
        }
    }
}