﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("新华社");
                DropDownList1.Items.Add("日报社");
                Label6.Text = "请输入新闻内容";
            }
            Label5.Text = System.DateTime.Now.ToShortTimeString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text==" ")
            {
                TextBox1.Text = "请提交新闻";
            }
            Label6.Text = $"此新闻来自{DropDownList1.Text}谢谢您提交,";
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            TextBox1.Text = "";
            Label6.Text = "请输入新闻内容";
        }
    }
}