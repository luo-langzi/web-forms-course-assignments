﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.DropDownList1.Items.Add("新华社");
                this.DropDownList1.Items.Add("图书社");
                this.DropDownList1.Items.Add("动漫社");
                this.Label5.Text = DateTime.Now.ToLongDateString().ToString();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1 == null)
            {
                Response.Write("请输入新闻内容");
            }
            else
            {
                Label6.Text = "此新闻来自"+ DropDownList1.Text + "，谢谢您提交的新闻。"; 
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            TextBox1.Text = null;
            DropDownList1.Text = "新华社";
        }
    }
}