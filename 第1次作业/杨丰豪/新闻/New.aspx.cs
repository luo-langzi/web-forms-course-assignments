﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication3
{
    public partial class New : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label3.Text = System.DateTime.Now.ToString();
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            TextBox1.Text ="";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text == "")
            {
                Label5.Text = $"请输入新闻内容";
            }
            else
            {
                Label5.Text = $"此新闻来自{DropDownList1.SelectedValue}谢谢您提交的新闻";
            }
        }
    }
}