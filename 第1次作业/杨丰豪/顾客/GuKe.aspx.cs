﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication4
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label5.Text = System.DateTime.Now.ToString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label6.Text = "请确认您填写的信息：";
            Label7.Text = $"姓名:{TextBox1.Text}";
            Label8.Text = $"年龄：{TextBox2.Text}";
            Label9.Text = $"爱好：{TextBox3.Text}";
        }
    }
}