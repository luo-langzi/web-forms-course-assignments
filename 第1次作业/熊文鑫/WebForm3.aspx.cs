﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label5.Text = System.DateTime.Now.ToShortTimeString();
            }
        }

        protected void TextBox4_TextChanged(object sender, EventArgs e)
        {
            TextBox1.Text = $"姓名：{TextBox2.Text} 年龄：{TextBox2.Text}" +
                $"爱好：{TextBox3.Text}";
        }
    }
}