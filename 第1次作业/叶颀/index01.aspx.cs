﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class index01 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Label5.Text = DateTime.Now.ToLongDateString().ToString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1 == null)
            {
                Label6.Text="请输入新闻内容";
            }
            else
            {
                Label6.Text = "提交成功";
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            TextBox1.Text = " ";
            DropDownList1.Text = "新华社";
        }
    }
}