﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.TextSex.Items.Add("男");
                this.TextSex.Items.Add("女");
            }
        }
        protected void Button1_Click1(object sender, EventArgs e)
        {
            Label5.Text = $"你的姓名为:{TextName.Text} 你的年龄是：{TextAge.Text} 你的性别是:{TextSex.Text} 你的爱好是:{TextHobby.Text}";
        }
    }
}