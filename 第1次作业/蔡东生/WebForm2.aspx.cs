﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) { 
            DropDownList1.Items.Add("新华社");
            DropDownList1.Items.Add("巨婴社");
            DropDownList1.Items.Add("华子社");
           }
            TextBox2.Text = System.DateTime.Now.ToShortDateString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                TextBox3.Text = $"此新闻来自{DropDownList1.SelectedValue}谢谢您提交的新闻";

            }
            else
            {
                TextBox3.Text = "请输入新闻内容";
            }
            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            TextBox1.Text = null;
        }
    }
}