﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TextBox1.Text = "请输入";
            }
            #region  //if (!Page.IsPostBack)
            //{
            //    ListItem item0 = new ListItem("男");//"10"为item0项的关联值
            //    ListItem item1 = new ListItem("女");//"11"为item1项的关联值

            //    this.DropDownList1.Items.Add(item0);//添加到DropDownList1
            //    this.DropDownList1.Items.Add(item1);//添加到DropDownList1

            //    //添加项第一种方法：
            //    //ListItem item4 = new ListItem("插入选项1", "1");//"1"为插入位置的索引
            //    //this.DropDownList1.Items.Insert(2, item4);//插入到指定位置

            //    //添加项第二种方法：
            //    //ListItem ltCu = new ListItem();
            //    //ltCu.Value = "3";//关联的值
            //    //ltCu.Text = "插入项2";//显示的文字
            //    //this.DropDownList1.Items.Add(ltCu);//添加到DropDownList1

            //    ////添加项第三种方法
            //    //this.DropDownList1.Items.Insert(0, new ListItem("-请选择-", "-1")); //"0"为插入位置的索;"-请选择-"为显示时的文字;"-1"为关联的值
            #endregion //}
            TextBox3_TextChanged(sender, e);
            DropDownList1_SelectedIndexChanged(sender, e);
        }

        protected void TextBox3_TextChanged(object sender, EventArgs e)
        {
            TextBox3.Text = TextBox1.Text;
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ListItem item0 = new ListItem("男");//"10"为item0项的关联值
                ListItem item1 = new ListItem("女");//"11"为item1项的关联值

                this.DropDownList1.Items.Add(item0);//添加到DropDownList1
                this.DropDownList1.Items.Add(item1);//添加到DropDownList1
            }
        }
    }
}