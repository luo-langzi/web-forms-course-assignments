﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="在线新闻发布系统" Font-Size="Large" Font-Bold="True" Font-Overline="False" ForeColor="Black"></asp:Label>
            <br />
            <asp:Label ID="Label2" runat="server" Text="新闻来源："></asp:Label><asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
            <br />
            <asp:Label ID="Label3" runat="server" Text="新闻内容："></asp:Label><asp:TextBox ID="TextBox1" runat="server" Columns="8" Height="220px" Rows="18" Width="190px"></asp:TextBox>
            <br />
            <asp:Label ID="Label4" runat="server" Text="发布时间："></asp:Label><asp:Label ID="Label5" runat="server" Text=""></asp:Label>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" /><asp:Button ID="Button2" runat="server" Text="重置" OnClick="Button2_Click" />
            <br />
            <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
