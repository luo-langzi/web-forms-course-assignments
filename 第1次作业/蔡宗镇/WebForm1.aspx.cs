﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DropDownList2.Items.Add("boy");
                DropDownList2.Items.Add("girl");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label5.Text = $"姓名：{TextBox1.Text}年龄：{TextBox2.Text}性别：{DropDownList2.SelectedValue} 爱好：{TextBox3.Text}";
        }
    }
}