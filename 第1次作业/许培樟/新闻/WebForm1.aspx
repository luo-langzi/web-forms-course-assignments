﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label1" runat="server" Text="在线新闻发布系统"></asp:Label>
            <br />
            <asp:Label ID="Label2" runat="server" Text="新闻来源"></asp:Label><asp:DropDownList ID="DropDownList1" runat="server">
            <asp:ListItem>新华社</asp:ListItem>
            <asp:ListItem>清华社</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:Label ID="Label3" runat="server" Text="新闻内容"></asp:Label><asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Rows="10" Columns="21"></asp:TextBox>
            <br />
            <asp:Label ID="Label4" runat="server" Text="发布时间"></asp:Label>
            <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />&nbsp; <asp:Button ID="Button2" runat="server" Text="重置" OnClick="Button2_Click" />
            <br />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
