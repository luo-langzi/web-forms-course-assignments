﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication2.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h3><asp:Label ID="Label1" runat="server" Text="在线新闻发布系统"></asp:Label><h3>
           <br />
            <asp:Label ID="Label3" runat="server" Text="新闻来源"></asp:Label>
           <br />
            <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                	<asp:ListItem Selected="True">新华社</asp:ListItem>
                	<asp:ListItem>南京社</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:Label ID="Label2" runat="server" Text="新闻内容"></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Columns="100" Rows="50"></asp:TextBox>
            <br />
            <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <asp:Button ID="Button2" runat="server" Text="重置" OnClick="Button2_Click" />
            <br />
            <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
