﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string Time = System.DateTime.Now.ToString();
                Label4.Text = "发布时间"+Time;
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList1.Items.Add("北师大出版社");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1 != null)
            {
                Label5.Text = $"此新闻来自 {DropDownList1.Text} 谢谢您提交的新闻！";
            }
            else
            {
                Label5.Text = "请输入新闻内容";
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            TextBox1.Text = "";
        }
    }
}