﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" font-size="20" />
    <title></title>
    <style type="text/css" >
        .label3 {
            margin-top:50px;
            margin-bottom:50px;
        }
    </style>
</head>
    
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="顾客信息登记表" Font-Bold="True" Font-Size="20pt"></asp:Label>
            <br />
            <asp:Label ID="Label2" runat="server" Text="姓名："></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="Label3" runat="server" Text="年龄："></asp:Label><asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="Label4" runat="server" Text="爱好："></asp:Label><asp:TextBox ID="TextBox3" runat="server" Height="200px"></asp:TextBox>
            <br />
            <asp:Label ID="Label5" runat="server" Text="" Width="155px"></asp:Label><asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            <asp:Label ID="Label6" runat="server" Text="您好，当前时间是："></asp:Label><asp:Label ID="Label7" runat="server"></asp:Label>
            <br />
            <asp:TextBox ID="TextBox4" runat="server" Height="100px" Width="300px" Rows="8" TextMode="MultiLine"></asp:TextBox>
        </div>
    </form>
</body>
</html>
