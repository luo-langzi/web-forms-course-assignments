﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack==false)
            {
                Label7.Text = System.DateTime.Now.ToShortDateString();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            TextBox4.Text = $"\t请确认您填写的消息：\n"+""+ "\t姓名:" + TextBox1.Text+"\n"+"\t年龄："+TextBox2.Text+"\n"+"\t爱好:"+TextBox3.Text;
        }
    }
}