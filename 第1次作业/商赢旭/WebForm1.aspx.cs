﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TextBox1.Text == "")
            {
                Label3.Text = "请输入新闻内容";
            }
            else if (TextBox1.Text != "")
            {
                Label3.Text = ($"此新闻来自{DropDownList1.SelectedValue}谢谢您提交的新闻");
            }
        }
    }
}