﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication4
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("福建");
                DropDownList1.Items.Add("广西");
                DropDownList1.Items.Add("广东");
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList2.Items.Clear();
            if (DropDownList1.SelectedValue == "福建")
            {
                DropDownList2.Items.Add("福州");
                DropDownList2.Items.Add("龙岩");
                DropDownList2.Items.Add("三明");
            }
            else if (DropDownList1.SelectedValue == "广西")
            {
                DropDownList2.Items.Add("南宁");
                DropDownList2.Items.Add("桂林");
                DropDownList2.Items.Add("柳州");
            }
            else
            {
                DropDownList2.Items.Add("深圳");
                DropDownList2.Items.Add("佛山");
                DropDownList2.Items.Add("东莞");
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            //Response.Write(DropDownList1.SelectedValue);
            //Response.Write(DropDownList2.SelectedValue);
            //Response.Write(RadioButtonList1.SelectedValue);
            string s1 = "";
            string s2 = "";
            string s3 = "";
            string s4 = "";
            string s5 = "";
            string s6 = "";
            for (int i = 0; i < DropDownList1.Items.Count; i++)
            {
                if (DropDownList1.Items[i].Selected)//省
                {
                    s1 += DropDownList1.Items[i].Value;
                }
            }
            for (int i = 0; i < DropDownList2.Items.Count; i++)
            {
                if (DropDownList2.Items[i].Selected)//市
                {
                    s2 += DropDownList2.Items[i].Value;
                }
            }
            for (int i = 0; i < CheckBoxList1.Items.Count; i++)
            {
                if (CheckBoxList1.Items[i].Selected)//爱好
                {
                    s3 += CheckBoxList1.Items[i].Value + " ";
                }
            }
            for (int i = 0; i < RadioButtonList1.Items.Count; i++)
            {
                if (RadioButtonList1.Items[i].Selected)//性别
                {
                    s4 += RadioButtonList1.Items[i].Value;
                }
            }
            for (int i = 0; i < TextBox3.Text.Length; i++)
            {
                //if (TextBox3.Text)//用户其他信息
                //{
                s5 = TextBox3.Text;
                //}
            }
            for (int i = 0; i < TextBox1.Text.Length; i++)
            {
                //if (TextBox1.Text)//用户名字
                //{
                s6 = TextBox1.Text;
                //}
            }
            Literal0.Text = "用户信息如下：";
            Literal1.Text = "用户名：" + s6;
            Literal2.Text = "性别：" + s4;
            Literal3.Text = "爱好：" + s3;
            Literal4.Text = $"籍贯：{s1}省{s2}市";
            Literal5.Text = "其他信息：" + s5;

        }
    }
}