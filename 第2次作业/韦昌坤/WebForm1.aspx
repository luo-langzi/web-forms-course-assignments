﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication4.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            用户名：<asp:TextBox ID="TextBox1" runat="server" style="margin-left: 8px"></asp:TextBox><br/>
            密码：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password" style="margin-left: 10px" Width="200px"></asp:TextBox><br/>
            性别：<asp:RadioButtonList ID="RadioButtonList1" runat="server">
                <asp:ListItem>男</asp:ListItem>
                <asp:ListItem>女</asp:ListItem>
            </asp:RadioButtonList><br/>
            爱好：<asp:CheckBoxList ID="CheckBoxList1" runat="server">
                <asp:ListItem>跳广场舞</asp:ListItem>
                <asp:ListItem>蹦迪</asp:ListItem>
                <asp:ListItem>敲代码</asp:ListItem>
                <asp:ListItem>其他</asp:ListItem>
            </asp:CheckBoxList><br/>
            籍贯：
            <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem>福建</asp:ListItem>
                <asp:ListItem>广西</asp:ListItem>
                <asp:ListItem>广东</asp:ListItem>
            </asp:DropDownList>省
            <asp:DropDownList ID="DropDownList2" runat="server">
              <asp:ListItem>福州</asp:ListItem>
                <asp:ListItem>龙岩</asp:ListItem>
                <asp:ListItem>三明</asp:ListItem>
                <asp:ListItem>南宁</asp:ListItem>
                <asp:ListItem>桂林</asp:ListItem>
                <asp:ListItem>柳州</asp:ListItem>
                <asp:ListItem>深圳</asp:ListItem>
                <asp:ListItem>佛山</asp:ListItem>
                <asp:ListItem>东莞</asp:ListItem>
            </asp:DropDownList>市<br/><br/>
            其他信息：<asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine"></asp:TextBox><br/><br/>
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" style="margin-left: 235px" Width="67px"/><br/>
            <asp:Literal ID="Literal0" runat="server"></asp:Literal><br/>
            <asp:Literal ID="Literal1" runat="server"></asp:Literal><br/>
            <asp:Literal ID="Literal2" runat="server"></asp:Literal><br/>
            <asp:Literal ID="Literal3" runat="server"></asp:Literal><br/>
            <asp:Literal ID="Literal4" runat="server"></asp:Literal><br/>
            <asp:Literal ID="Literal5" runat="server"></asp:Literal><br/>
        </div>
    </form>
</body>
</html>
