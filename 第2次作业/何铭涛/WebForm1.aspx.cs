﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

            this.TextBox2.Attributes.Add("value", TextBox2.Text);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string sex="";
            if (RadioButton1.Checked==true)
            {
                sex = RadioButton1.Text;
            }
            else if (RadioButton2.Checked==true)
            {
                sex = RadioButton2.Text;
            }

            TextBox6.Text = "\t" + "用户名:" + TextBox1.Text + "\n" + "\t" + "密码:" + TextBox2.Text + "\n" + "\t" + "性别："+sex+"\n"+"\t"+"爱好："+TextBox4.Text+"\n"+"\t"+DropDownList1.SelectedValue+"省"+DropDownList2.SelectedValue+"市"+"\n"+"\t"+TextBox5.Text;
        }


        protected void DropDownList1_TextChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            { 
                if (DropDownList1.SelectedValue == "福建")
                {
                    DropDownList2.Items.Clear();
                    DropDownList2.Items.Add("厦门");
                    DropDownList2.Items.Add("宁德");
                    DropDownList2.Items.Add("龙岩");

                }
                if (DropDownList1.SelectedValue == "江西")
                {
                    DropDownList2.Items.Clear();
                    DropDownList2.Items.Add("赣州");
                    DropDownList2.Items.Add("九江");
                    DropDownList2.Items.Add("南昌");
                }
                if (DropDownList1.SelectedValue == "河南")
                {
                    DropDownList2.Items.Clear();
                    DropDownList2.Items.Add("郑州");
                    DropDownList2.Items.Add("洛阳");
                    DropDownList2.Items.Add("开封");
                }   
            }
        }
    }
}