﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {

            }
        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList2.Equals("浙江"))
            {
                DropDownList3.Items.Add("宁波");
                DropDownList3.Items.Add("杭州");
                DropDownList3.Items.Add("龙泉");
            DropDownList3.Items.Clear();
            }
            else if (DropDownList2.Equals("福建"))
            {
                DropDownList3.Items.Add("福州");
                DropDownList3.Items.Add("龙岩");
                DropDownList3.Items.Add("厦门");
            }
            else if (DropDownList3.Equals("江西"))
            {
                DropDownList3.Items.Add("南昌");
                DropDownList3.Items.Add("九江");
                DropDownList3.Items.Add("吉安");
            }
            else if (DropDownList3.Equals("湖南"))
            {
                DropDownList3.Items.Add("长沙");
                DropDownList3.Items.Add("张家界");
                DropDownList3.Items.Add("岳阳");
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
                DropDownList3.Items.Clear();
            if (DropDownList2.Text.Equals("浙江"))
            {
                DropDownList3.Items.Add("宁波");
                DropDownList3.Items.Add("杭州");
                DropDownList3.Items.Add("龙泉");
            }
            else if (DropDownList2.Text.Equals("福建"))
            {
                DropDownList3.Items.Add("福州");
                DropDownList3.Items.Add("龙岩");
                DropDownList3.Items.Add("厦门");
            }
            else if (DropDownList2.Text.Equals("江西"))
            {
                DropDownList3.Items.Add("南昌");
                DropDownList3.Items.Add("九江");
                DropDownList3.Items.Add("吉安");
            }
            else if (DropDownList2.Text.Equals("湖南"))
            {
                DropDownList3.Items.Add("长沙");
                DropDownList3.Items.Add("张家界");
                DropDownList3.Items.Add("岳阳");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1 != null && TextBox2 != null && TextBox3 != null && TextBox4 != null)
            {
                TextBox5.Text = $"用户名：{TextBox1.Text}\r\n性别：{DropDownList1.Text}\r\n爱好：{TextBox3.Text}\r\n籍贯：{DropDownList2.Text}省{DropDownList3.Text}\r\n其他信息：{TextBox4.Text}";
            }
        }
    }
}