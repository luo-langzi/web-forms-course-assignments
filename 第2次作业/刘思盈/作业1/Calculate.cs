﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    //{一、编写一个控制台应用程序，接受用户输入的两个整数和一个操作符，以实现对两个整数的加、减、乘、除运算，并显示出计算结果。

    //1、创建Calculate积累，其中包含两个整型的protected成员，用以接收用户输入的两个整数。定义一个DisplayResult()虚方法，计算并输出结果。
    //2、定义四个类继承自Calculate类，分别重写DisplayResult()方法，实现两个整数的加、减、乘、除运算，并输出结果。
    //3、根据用户输入的操作符，实例化相应的类，完成运算并输出结果。
    //4、在主类中添加一个方法，形参为父类对象，根据传递实参的类型，调用方法，实现计算和显示结果。
    class Calculate
    {
        private int a;
        private int b;

        public int A { get => a; set => a = value; }
        public int B { get => b; set => b = value; }
        public Calculate(int a,int b) {
            this.A = a;
            this.B = b;
        }

        public virtual void DisplayResult()
        {
            }
        }


    
}
