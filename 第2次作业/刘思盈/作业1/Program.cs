﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    //{一、编写一个控制台应用程序，接受用户输入的两个整数和一个操作符，以实现对两个整数的加、减、乘、除运算，并显示出计算结果。

    //1、创建Calculate积累，其中包含两个整型的protected成员，用以接收用户输入的两个整数。定义一个DisplayResult()虚方法，计算并输出结果。
    //2、定义四个类继承自Calculate类，分别重写DisplayResult()方法，实现两个整数的加、减、乘、除运算，并输出结果。
    //3、根据用户输入的操作符，实例化相应的类，完成运算并输出结果。
    //4、在主类中添加一个方法，形参为父类对象，根据传递实参的类型，调用方法，实现计算和显示结果。

    class Program
    {
        static void Main(string[] args)
        {

          

            Console.WriteLine("请输入第一个整数的值");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入第二个整数的值");
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入运算符：");
            string f = Console.ReadLine();
            Calculate calculate = new Calculate(a,b);
            switch (f)
            {
                case "+":
                    Class1 class1 = new Class1(a,b);
                    class1.DisplayResult();
                    break;
                case "-":
                    Class2 class2 = new Class2(a, b);
                    class2.DisplayResult();
                    break;
                case "*":
                    Class3 class3 = new Class3(a,b);
                    class3.DisplayResult();

                    break;
                case "/":
                    Class4 class4 = new Class4(a, b);
                    class4.DisplayResult();
                    break;
                default:
                    break;
            }
        }
    }
}