﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    class Square:Shape
    {
       
        private double sideLen;


        public double SideLen { get => sideLen; set => sideLen = value; }





        public Square(double sideLen)
        {
            this.sideLen = sideLen;
        }

        public override void GetArea()
        {
            base.GetArea();
            Console.WriteLine("正方形的面积为：" + (sideLen * sideLen));

        }

    }
}
