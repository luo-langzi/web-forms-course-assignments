﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    class Circle:Shape
    {
        private double radius;
       

        public double Radius { get => radius; set => radius = value; }
 

        

       
        public Circle(double radius)
        {
            this.radius = radius;
        }

        public override void GetArea()
        {
            base.GetArea();
            Console.WriteLine("圆的面积为："+(3.14*radius*radius));

        }

    }
}
