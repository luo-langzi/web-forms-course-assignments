﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("广东");
                DropDownList1.Items.Add("浙江");
                DropDownList1.Items.Add("福建");
            }
            DropDownList2.Items.Clear();

        }

        

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
           
            if (DropDownList1.SelectedValue == "广东")
            {
                DropDownList2.Items.Add("广州");
                DropDownList2.Items.Add("佛山");
                DropDownList2.Items.Add("深圳");
            }
            else if (DropDownList1.SelectedValue == "浙江")
            {
                DropDownList2.Items.Add("杭州");
                DropDownList2.Items.Add("义乌");
                DropDownList2.Items.Add("温州");
            }
            else if (DropDownList1.SelectedValue == "福建")
            {
                DropDownList2.Items.Add("泉州");
                DropDownList2.Items.Add("厦门");
                DropDownList2.Items.Add("漳州");

            }

        }

 

        protected void Button1_Click(object sender, EventArgs e)
        {
            Literal2.Text = $"用户名：{TextBox1.Text} \r\n 性别：{RadioButtonList1.SelectedValue} \r\n 爱好：{TextBox4.Text} \r\n 籍贯：{DropDownList1.SelectedValue}省{DropDownList2.SelectedValue}市";
        }
    }
}