﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="WebApplication1.WebForm3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Literal ID="Literal1" runat="server" Text="注册页面"></asp:Literal>
            <br />
            用户名： <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
             
            <br />
            密码：&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
             
            <br />
            性别：<asp:RadioButtonList ID="RadioButtonList1" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" DataValueField="男" Width="76px">
                <asp:ListItem >男</asp:ListItem><asp:ListItem>女</asp:ListItem>
                
            </asp:RadioButtonList>
           
            <br />
            爱好： &nbsp; <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
             
            <br />
            籍贯： &nbsp; <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" AutoPostBack="true">
              
            </asp:DropDownList>
            <asp:DropDownList ID="DropDownList2" runat="server" >
                <asp:ListItem>广州</asp:ListItem>
                <asp:ListItem>佛山</asp:ListItem>
                <asp:ListItem>深圳</asp:ListItem>
                <asp:ListItem>杭州</asp:ListItem>
                <asp:ListItem>义乌</asp:ListItem>
                <asp:ListItem>温州</asp:ListItem>
                <asp:ListItem>泉州</asp:ListItem>
                <asp:ListItem>厦门</asp:ListItem>
                <asp:ListItem>漳州</asp:ListItem>
                <asp:ListItem></asp:ListItem>
            </asp:DropDownList>
            <br />
            其他信息：<asp:TextBox ID="TextBox3" runat="server" Rows="10" Columns="8" TextMode="MultiLine" Width="156px"></asp:TextBox>
            <br />
&nbsp;<asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            <asp:Literal ID="Literal2" runat="server"></asp:Literal>
            <br />
            <asp:Literal ID="Literal3" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
