﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="WebApplication1.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            用户名：<asp:TextBox ID="TextBox1" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <br />
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <br />
            <br />
            性别：<asp:RadioButtonList ID="RadioButtonList1" runat="server" Width="74px">
                <asp:ListItem>男</asp:ListItem>
                <asp:ListItem>女</asp:ListItem>
            </asp:RadioButtonList>
            爱好：<asp:CheckBoxList ID="CheckBoxList1" runat="server">
                <asp:ListItem>敲代码</asp:ListItem>
                <asp:ListItem>吃饭</asp:ListItem>
                <asp:ListItem>喝酒</asp:ListItem>
                <asp:ListItem>探头</asp:ListItem>
            </asp:CheckBoxList>
            籍贯：<asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True" OnTextChanged="DropDownList1_TextChanged">
               
            </asp:DropDownList>省
            <asp:DropDownList ID="DropDownList2" runat="server" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
              
            </asp:DropDownList>市<br />
            其它信息：<asp:TextBox ID="TextBox3" runat="server" Height="82px" TextMode="MultiLine" Width="163px"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            <asp:TextBox ID="TextBox4" runat="server" Height="98px" TextMode="MultiLine" Width="190px"></asp:TextBox>
    </form>
    </body>
</html>
