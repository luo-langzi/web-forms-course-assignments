﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("福建");
                DropDownList1.Items.Add("广东");

            }
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string str = "";
            for (int i = 0; i < CheckBoxList1.Items.Count; i++)
            {
                str += "" + CheckBoxList1.Items[i];
            }
            TextBox4.Text = $"用户名：{TextBox1.Text}\n密码：{TextBox2.Text}\n性别：{RadioButtonList1.SelectedValue}\n" +
                $"爱好："+str;
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        protected void DropDownList1_TextChanged(object sender, EventArgs e)
        {
            DropDownList2.Items.Clear();
            if (DropDownList1.SelectedValue == "福建")
            {
                DropDownList2.Items.Add("龙岩");
                DropDownList2.Items.Add("南平");
                DropDownList2.Items.Add("泉州");

            }
            else if (DropDownList1.SelectedValue == "广东")
            {
                DropDownList2.Items.Add("广州");
                DropDownList2.Items.Add("深圳");
                DropDownList2.Items.Add("广西");
            }
        }
    }
}