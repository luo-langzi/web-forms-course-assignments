﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class index01 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("福建");
                DropDownList1.Items.Add("广东");
                DropDownList1.Items.Add("广西");

            }

        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList2.Items.Clear();
            if (DropDownList1.SelectedValue == "福建")
            {
                DropDownList2.Items.Add("福州");
                DropDownList2.Items.Add("厦门");
                DropDownList2.Items.Add("漳州");

            }
            else if (DropDownList1.SelectedValue == "广东")
            {
                DropDownList2.Items.Add("广州");
                DropDownList2.Items.Add("韶关");
                DropDownList2.Items.Add("阳江");

            }
            else if (DropDownList1.SelectedValue == "广西")
            {
                DropDownList2.Items.Add("博白");
                DropDownList2.Items.Add("南宁");
                DropDownList2.Items.Add("阳江");

            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label12.Text = $"用户名{ TextBox1.Text},爱好{TextBox3.Text},其它信息{TextBox4.Text}";
        }
    }
}