﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("福建");
                DropDownList1.Items.Add("广东");
                DropDownList1.Items.Add("广西");
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList3.Items.Clear();
            if (DropDownList1.SelectedValue == "福建")
            {
                DropDownList3.Items.Add("泉州");
                DropDownList3.Items.Add("龙岩");
                DropDownList3.Items.Add("三明");
            }
            else if (DropDownList1.SelectedValue == "广东")
            {
                DropDownList3.Items.Add("广州");
                DropDownList3.Items.Add("珠江");
                DropDownList3.Items.Add("深圳");
            }
            else if (DropDownList1.SelectedValue == "广西")
            {
                DropDownList3.Items.Add("博白");
                DropDownList3.Items.Add("凤凰");
                DropDownList3.Items.Add("广西");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string str = null;
            for (int i = 0; i < CheckBoxList1.Items.Count; i++)
            {
                if (CheckBoxList1.Items[i].Selected)
                {
                    str = CheckBoxList1.Items[i] + "";
                }
            }
            Literal2.Text = $"姓名：{TextBox1.Text}年龄：{TextBox2.Text}性别：{DropDownList2.SelectedValue} 爱好：{CheckBoxList1.Text} 籍贯 省:{DropDownList1.Text} 市：{DropDownList3.Text}其他信息:{TextBox4.Text}";
        }
        


    }
}