﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             注册账号
            <br />
            用户名<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <br />
            性别:<asp:DropDownList ID="DropDownList2" runat="server">
                <asp:ListItem>男</asp:ListItem>
                <asp:ListItem>女</asp:ListItem>
               </asp:DropDownList>
            <br />
            <asp:Label ID="Label1" runat="server" Text="爱好:"></asp:Label>
            <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                <asp:ListItem>打篮球</asp:ListItem>
                <asp:ListItem>踢足球</asp:ListItem>
                <asp:ListItem>游泳</asp:ListItem>
                <asp:ListItem>翼装飞行</asp:ListItem>
            </asp:CheckBoxList>
            <br />
            省：<asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            <br />
            市区：<asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList>
            <br />
            其它信息：<asp:TextBox ID="TextBox4" runat="server" TextMode="MultiLine" Rows="8" MaxLength="8"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br/>
            <asp:Literal ID="Literal2" runat="server"></asp:Literal>
            <br/>
            
            
        </div>
    </form>
</body>
</html>
