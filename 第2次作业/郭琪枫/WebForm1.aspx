﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1 style="text-align:center">用户注册</h1><br />
            用户名：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            性别：<asp:RadioButtonList ID="RadioButtonList1" runat="server">
                <asp:ListItem>男</asp:ListItem>
                <asp:ListItem>女</asp:ListItem>
            </asp:RadioButtonList>
            爱好：<asp:TextBox ID="TextBox3" runat="server" Columns="20" Rows="10" TextMode="MultiLine"></asp:TextBox>
            <br />
            籍贯：省：<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" >
            </asp:DropDownList>
            <br />
&nbsp;&nbsp;&nbsp;市：<asp:DropDownList ID="DropDownList2" runat="server"  >
                <asp:ListItem>厦门</asp:ListItem>
                <asp:ListItem>泉州</asp:ListItem>
                <asp:ListItem>福州</asp:ListItem>
                <asp:ListItem>长沙</asp:ListItem>
                <asp:ListItem>株洲</asp:ListItem>
                <asp:ListItem>湘潭</asp:ListItem>
                <asp:ListItem>武汉</asp:ListItem>
                <asp:ListItem>十堰</asp:ListItem>
                <asp:ListItem>宜昌</asp:ListItem>
            </asp:DropDownList>
                       <br />
            其他信息：<asp:TextBox ID="TextBox4" runat="server" TextMode="MultiLine" Rows="10" Columns="20"></asp:TextBox>
                       <br />
            <asp:Button ID="Button1" runat="server" Text="提交" Width="82px" OnClick="Button1_Click" />
                       <br />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
