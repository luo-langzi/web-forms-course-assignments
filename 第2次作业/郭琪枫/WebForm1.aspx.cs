﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("福建");
                DropDownList1.Items.Add("湖南");
                DropDownList1.Items.Add("湖北");
            }
        }
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList2.Items.Clear();
            if (DropDownList1.SelectedValue == "福建")
            {
                DropDownList2.Items.Add("厦门");
                DropDownList2.Items.Add("泉州");
                DropDownList2.Items.Add("福州");
            }
            else if (DropDownList1.SelectedValue == "湖南")
            {
                DropDownList2.Items.Add("长沙");
                DropDownList2.Items.Add("株洲");
                DropDownList2.Items.Add("湘潭");
            }
            else
            {
                DropDownList2.Items.Add("武汉");
                DropDownList2.Items.Add("十堰");
                DropDownList2.Items.Add("宜昌");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Literal1.Text = $"您的用户名是{TextBox1.Text}，性别是{RadioButtonList1.SelectedValue}，爱好是{TextBox3.Text}，籍贯是{DropDownList1.SelectedValue}省，{DropDownList2.SelectedValue}市，其他信息：{TextBox4.Text}";
        }
    }
}