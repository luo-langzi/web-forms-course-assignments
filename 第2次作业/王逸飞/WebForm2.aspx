﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="WebApplication2.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="用户名："></asp:Label>&nbsp; <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:Label ID="Label2" runat="server" Text="密码："></asp:Label>&nbsp;&nbsp; <asp:TextBox ID="TextBox2" runat="server" Font-Names="幼圆" ReadOnly="True"></asp:TextBox>
        </div>
        <div>
            <asp:Label ID="Label7" runat="server" Text="性别："></asp:Label><asp:RadioButtonList ID="RadioButtonList1" runat="server">
                <asp:ListItem>男</asp:ListItem>
                <asp:ListItem>女</asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div>
            <asp:Label ID="Label3" runat="server" Text="爱好："></asp:Label>&nbsp;<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:Label ID="Label4" runat="server" Text="籍贯："></asp:Label>
            <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                <asp:ListItem Value="龙岩">福建</asp:ListItem>
                <asp:ListItem>河南</asp:ListItem>
                <asp:ListItem>河北</asp:ListItem>
                <asp:ListItem>山西</asp:ListItem>
            </asp:CheckBoxList>
        </div><div>
            <asp:Label ID="Label5" runat="server" Text="其他信息："></asp:Label>&nbsp;<asp:TextBox ID="TextBox5" runat="server" BorderStyle="None" OnTextChanged="TextBox5_TextChanged" TextMode="MultiLine"></asp:TextBox>
              </div>
        <div>
            <asp:Button ID="Button1" runat="server" Text="注册" />
            <br />
            <asp:Label ID="Label6" runat="server" Text="管理员提示"></asp:Label>
            <br />
            <asp:TextBox ID="TextBox6" runat="server" Enabled="False" Height="138px" TextMode="MultiLine" Width="406px" ReadOnly="True">阿萨打算打算打</asp:TextBox>
            <br />
            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/null-7cadb6ced1489374.jpg" style="margin-bottom: 0px" Width="412px" />
        </div>
    </form>
</body>
</html>
