﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication8.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
        }
    </style>
</head>
<body style="height: 192px">
    <form id="form1" runat="server">
       <div>
           <div style="text-align:center"><h1 class="auto-style1">注册页面</h1></div>
           <div style="text-align:center">
               <asp:Label ID="Label1" runat="server" Text="账号："></asp:Label><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></div>
       <div style="text-align:center">
           <asp:Label ID="Label2" runat="server" Text="密码："></asp:Label>
           <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
           </div>
           <div style="text-align:center; width: 1354px;">
               <asp:Label ID="Label3" runat="server" Text="性别："></asp:Label>
               </div>
            <div>
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" style="text-align: center" Width="1493px">
                    <asp:ListItem>男</asp:ListItem>
                    <asp:ListItem>女</asp:ListItem>
                </asp:RadioButtonList>
                </div>
           <div style="text-align:center">
               <asp:Label ID="Label4" runat="server" Text="爱好："></asp:Label><asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine"></asp:TextBox>
           </div>
           <div style="text-align:center">
               <asp:Label ID="Label5" runat="server" Text="籍贯："></asp:Label><asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged1" AutoPostBack="True"></asp:DropDownList>
               <asp:Label ID="Label6" runat="server" Text="市："></asp:Label>
               <asp:DropDownList ID="DropDownList2" runat="server">
               </asp:DropDownList>
           </div>
           <div style="text-align:center">
               <asp:Label ID="Label7" runat="server" Text="其他信息："></asp:Label><asp:TextBox ID="TextBox4" runat="server" TextMode="MultiLine"></asp:TextBox>
               </div>
            <div style="text-align:center">
                <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" Width="85px" />
                </div>
           <div style="text-align:center">
               <asp:Label ID="Label8" runat="server" Text="你所写的信息"></asp:Label>
               </div>
       </div>
    </form>
</body>
</html>
