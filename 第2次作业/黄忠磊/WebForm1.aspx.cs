﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                DropDownList1.Items.Add("河南省");
                DropDownList1.Items.Add("江苏省");
                DropDownList1.Items.Add("安徽省");
                RadioButtonList1.Items.Add("男");
                RadioButtonList1.Items.Add("女");
            }
            
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList2.Items.Clear();

            if (DropDownList1.SelectedValue=="河南省")
            {
                DropDownList2.Items.Add("信阳");
                DropDownList2.Items.Add("郑州");
                DropDownList2.Items.Add("洛阳");
            }

            else if (DropDownList1.SelectedValue== "广东省")
            {
                DropDownList2.Items.Add("广州");
                DropDownList2.Items.Add("深圳");
                DropDownList2.Items.Add("东莞");
            }
            else 
            {
                DropDownList2.Items.Add("无锡");
                DropDownList2.Items.Add("南京");
                DropDownList2.Items.Add("苏州");
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            TextBox4.Text = $"用户名：{TextBox1.Text};密码：{TextBox2.Text };性别：{RadioButtonList1.Text};爱好：{TextBox3.Text};省份：{DropDownList1.Text};市区：{DropDownList2.Text}；其它信息：{TextBox5.Text}";
        }

       
        }
    }
