﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>
<%--用户名：
密码：不能明文显示
性别：只能单选
爱好：
籍贯：省  市
其它信息：多行文本框
提交按钮

提交后，在下方显示提交的数据。--%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
用户名：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
密码：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <br />
性别：<asp:RadioButtonList ID="RadioButtonList1" runat="server">
    <asp:ListItem>男</asp:ListItem><asp:ListItem>女</asp:ListItem>
     </asp:RadioButtonList>
            <br />
爱好：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <br />
籍贯:
            省份:<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True"  OnTextChanged="DropDownList1_TextChanged" ></asp:DropDownList>
            市区:<asp:DropDownList ID="DropDownList2" runat="server"></asp:DropDownList>
            <br />
其它信息：<asp:TextBox ID="TextBox4" runat="server" Height="200px" TextMode="MultiLine" Width="200px"></asp:TextBox>
            <br />
<asp:Button ID="Button1" runat="server" Text="提交按钮" OnClick="Button1_Click" />
            <br />
            <asp:TextBox ID="TextBox5" runat="server" Height="92px" Width="259px"></asp:TextBox>

        </div>
    </form>
</body>
</html>
