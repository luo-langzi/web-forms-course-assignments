﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.TextBox2.Attributes.Add("value", TextBox2.Text);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string sex = "";
            if (RadioButton1.Checked == true)
            {
                sex = RadioButton1.Text;
            }
            else if (RadioButton2.Checked == true)
            {
                sex = RadioButton2.Text;
            }

            Label7.Text = "\t" + "用户名:" + TextBox1.Text + "\n" + "\t" + "密码:" + TextBox2.Text + "\n" + "\t" + "性别：" + sex + "\n" + "\t" + "爱好：" + TextBox4.Text + "\n" + "\t" + DropDownList1.SelectedValue + "省" + DropDownList2.SelectedValue + "市" + "\n" + "\t" + TextBox4.Text;
        }

        protected void DropDownList1_TextChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (DropDownList1.SelectedValue == "福建")
                {
                    DropDownList2.Items.Clear();
                    DropDownList2.Items.Add("厦门");
                    DropDownList2.Items.Add("福州");
                }
                if (DropDownList1.SelectedValue == "广东")
                {
                    DropDownList2.Items.Clear();
                    DropDownList2.Items.Add("深圳");
                    DropDownList2.Items.Add("珠海");
                }
            }
        }
    }
}