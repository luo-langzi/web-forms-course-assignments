﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("福建");
                DropDownList1.Items.Add("贵州"); 
                DropDownList1.Items.Add("四川");
            }
            
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue=="福建")
            {
                DropDownList2.Items.Add("三明");
                DropDownList2.Items.Add("晋江");
                DropDownList2.Items.Add("龙岩");
            }
            else if (DropDownList1.SelectedValue=="贵州")
            {
                DropDownList2.Items.Add("六盘水");
                DropDownList2.Items.Add("贵阳");
                DropDownList2.Items.Add("毕节");
            }
            else if (DropDownList1.SelectedValue=="四川")
            {
                DropDownList2.Items.Add("达州");
                DropDownList2.Items.Add("广元");
                DropDownList2.Items.Add("成都");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label1.Text=$"姓名是:{TextBox1.Text} 密码是:{TextBox2.Text}性别是:{RadioButtonList1.SelectedValue}爱好:{TextBox3.Text}籍贯:{DropDownList1.SelectedValue}{DropDownList2.SelectedValue}其他信息:{TextBox4.Text} ";
        }
    }
}