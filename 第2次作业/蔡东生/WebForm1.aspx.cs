﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("男");
                DropDownList1.Items.Add("女");
                DropDownList1.Items.Add("人妖");
                DropDownList1.Items.Add("华子");
                DropDownList1.Items.Add("秀吉");
                DropDownList2.Items.Add("福建");
                DropDownList2.Items.Add("海南");
                DropDownList2.Items.Add("湖北");
                CheckBoxList1.Items.Add("天地玄黄");
                CheckBoxList1.Items.Add("地玄黄");
                CheckBoxList1.Items.Add("玄黄");
                CheckBoxList1.Items.Add("黄");

            }
            this.TextBox2.Attributes.Add("value", TextBox2.Text);
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList3.Items.Clear();
            if (DropDownList2.SelectedValue == "福建")
            {
                DropDownList3.Items.Add("厦门");
                DropDownList3.Items.Add("泉州");
                DropDownList3.Items.Add("龙岩");
                DropDownList3.Items.Add("福州");
            }
            else if (DropDownList2.SelectedValue == "海南")
            {
                DropDownList3.Items.Add("三亚");
                DropDownList3.Items.Add("泉州");
                DropDownList3.Items.Add("龙岩");
                DropDownList3.Items.Add("福州");
            }
            else if (DropDownList2.SelectedValue == "湖北")
            {
                DropDownList3.Items.Add("武汉");
                DropDownList3.Items.Add("泉州");
                DropDownList3.Items.Add("龙岩");
                DropDownList3.Items.Add("福州");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string str = "";
            for (int i = 0; i < CheckBoxList1.Items.Count; i++)
            {
                if (CheckBoxList1.Items[i].Selected)
                {
                    str += " " + CheckBoxList1.Items[i];
                }
            }
            TextBox4.Text=$"用户名：{TextBox1.Text},性别：{DropDownList1.Text},省：{DropDownList2.Text},市：{DropDownList3.Text},爱好：{str},其它：{TextBox3.Text}";
            
        }
    }
}