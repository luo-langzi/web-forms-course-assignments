﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div> 用户名：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            <br />
            性别：<asp:DropDownList ID="DropDownList1" runat="server">
            </asp:DropDownList>
            <br />
&nbsp;<br />
            爱好：<asp:CheckBoxList ID="CheckBoxList1" runat="server"></asp:CheckBoxList>
            <br />
            <br />
            籍贯：<asp:DropDownList ID="DropDownList2" runat="server" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>省<asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList>市
            <br />        
            <br />
            其它信息：<asp:TextBox ID="TextBox3" runat="server" Rows="15" TextMode="MultiLine"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            <asp:TextBox ID="TextBox4" runat="server" Rows="20" TextMode="MultiLine"></asp:TextBox>
        </div>
    </form>
</body>
</html>
