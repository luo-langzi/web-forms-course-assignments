﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlPrice.Items.Add("福建");
                ddlPrice.Items.Add("广东");
                ddlPrice.Items.Add("浙江");
            }
          
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string str = "你选择的爱好是：";
            foreach (ListItem item in cblLove.Items)
            {
                if (item.Selected)
                {
                    str += item.Value + "、";
                }
            }
         
            Response.Write(string.Format("欢迎：{0}，你的性别是：{1}，{2}，地址是：{3}省，{4}市，其他信息:{5}",TextBox1.Text, RadioButtonList1.SelectedValue,str,ddlPrice.SelectedValue,ddlCity.SelectedValue,TextBox3.Text));
        }

        protected void cblLove_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlPrice_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlCity.Items.Clear();
            if (ddlPrice.SelectedValue == "福建")
            {
                ddlCity.Items.Add("厦门");
                ddlCity.Items.Add("泉州");
                ddlCity.Items.Add("漳州");
            }
            else if (ddlPrice.SelectedValue == "广东")
            {
                ddlCity.Items.Add("深圳");
                ddlCity.Items.Add("惠州");
                ddlCity.Items.Add("广州");
            }
            else if (ddlPrice.SelectedValue == "浙江")
            {
                ddlCity.Items.Add("温州");
                ddlCity.Items.Add("金华");
                ddlCity.Items.Add("杭州");
            }
        }
    }
}