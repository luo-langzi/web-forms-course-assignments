﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication2.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
  <form id="form1" runat="server">
        <div aria-controls="100">
          
<%--写一个注册界面
用户名：
密码：不能明文显示
性别：只能单选
爱好：
籍贯：省  市
其它信息：多行文本框
提交按钮
提交后，在下方显示提交的数据。--%>
            
           <%-- <asp:Label ID="Label1" runat="server" Text="注册页面"></asp:Label><br />

            <asp:Label ID="Label2" runat="server" Text="用户名："></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server"  TextMode="SingleLine" Width="200px" EnableViewState="False"></asp:TextBox><br />
     
            <asp:Label ID="Label3" runat="server" Text="密码："></asp:Label>
            <asp:TextBox ID="TextBox2" runat="server" TextMode="Password" Width="200px"></asp:TextBox><br />
              
            <asp:Label ID="Label4" runat="server" Text="性别："></asp:Label>
            <asp:RadioButtonList ID="RadioButtonList1" runat="server"></asp:RadioButtonList><br />
               
            <asp:Label ID="Label5" runat="server" Text="爱好："></asp:Label>
            <asp:CheckBoxList ID="CheckBoxList1" runat="server"></asp:CheckBoxList><br />--%>
               
            <asp:Label ID="Label2" runat="server" Text="注册页面"></asp:Label> 
            <br />
            用户名：
            <asp:TextBox ID="TextBox1" runat="server" TextMode="SingleLine" ></asp:TextBox>
            <br /> 
            密  码：
            <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID ="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox2" ErrorMessage ="密码不能为空" ></asp:RequiredFieldValidator>
            <br />
            性  别：
            <asp:RadioButtonList ID="RadioButtonList1" runat="server">
            <asp:listItem Selectsd =" Ture">男</asp:listItem>
            <asp:ListItem>女</asp:ListItem></asp:RadioButtonList>
            <br />
            爱  好：
            <asp:CheckBoxList ID="cblLove" runat="server" RepeatDirection ="Horizontal" RepeatLayout ="Flow" AutoPostBack ="true" OnSelectedIndexChanged ="cblLove_SelectedIndexChanged" >
                 <asp:ListItem Selected ="True">唱歌</asp:ListItem>
                 <asp:ListItem >篮球</asp:ListItem>
                 <asp:ListItem >爬山</asp:ListItem>
                 </asp:CheckBoxList>
            <br />
            籍  贯：
            <asp:DropDownList ID="ddlPrice" runat="server" AutoPostBack ="true" OnSelectedIndexChanged ="ddlPrice_SelectedIndexChanged">
            <asp:ListItem Text ="--请选择--"></asp:ListItem>
            </asp:DropDownList>
            <br/>
            城  市：
           <asp:DropDownList ID="ddlCity" runat="server"> <asp:ListItem Text ="--请选择--"></asp:ListItem></asp:DropDownList> 
           <br />
           其他信息：  
           <asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine"  Height="100px" Width="200px"></asp:TextBox>
           <br />
           <asp:Button ID="Button1" runat="server" Text="提交" OnClick ="Button1_Click" />

        </div>
</form>
</body>
</html>
