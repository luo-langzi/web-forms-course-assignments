﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            用户名：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />

            密&nbsp;&nbsp;&nbsp;码：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <br />

            性&nbsp;&nbsp;&nbsp;别：<asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
            <br />
            爱&nbsp;&nbsp;&nbsp;好：<asp:CheckBoxList ID="CheckBoxList1" runat="server"><asp:ListItem>打篮球</asp:ListItem><asp:ListItem>敲代码</asp:ListItem><asp:ListItem>跳舞</asp:ListItem></asp:CheckBoxList>
            <br />

            籍&nbsp;&nbsp;&nbsp;贯：
            <asp:DropDownList ID="DropDownList2" runat="server" AppendDataBoundItems="True" OnTextChanged="DropDownList2_TextChanged" AutoPostBack="True"></asp:DropDownList>省 
            <asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList>市

            <br />
            <br />

            其它信息：<asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine" Rows="8"></asp:TextBox>
            <br />
            <br />

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            <asp:Label ID="Label1" runat="server"></asp:Label>
            <input type="hidden" name="__VIEWSTATE" value="">
        </div>
    </form>
</body>
</html>
