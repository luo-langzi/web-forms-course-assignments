﻿using System;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                DropDownList1.Items.Add("男");
                DropDownList1.Items.Add("女");

                DropDownList2.Items.Add("福建");
                DropDownList2.Items.Add("湖北");

                DropDownList3.Items.Add("龙岩");
                DropDownList3.Items.Add("厦门");

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            string str = "";
            for (int i = 0; i < CheckBoxList1.Items.Count; i++)
            {
                if (CheckBoxList1.Items[i].Selected)
                {
                    str += " " + CheckBoxList1.Items[i].Value;
                }
            }
            Label1.Text = "用户名：" + TextBox1.Text + " 性别：" + DropDownList1.Text + " 爱好：" + str + " 籍贯：" + DropDownList2.Text + "省 " + DropDownList3.Text + "市";
        }

        protected void DropDownList2_TextChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (DropDownList2.SelectedValue == "福建")
                {
                    DropDownList3.Items.Clear();
                    DropDownList3.Items.Add("龙岩");
                    DropDownList3.Items.Add("厦门");
                }
                else if (DropDownList2.SelectedValue == "湖北")
                {
                    DropDownList3.Items.Clear();
                    DropDownList3.Items.Add("武汉");
                    DropDownList3.Items.Add("十堰");
                }
            }

        }
    }
}