﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication8
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("福建");
                DropDownList1.Items.Add("广西");
            }
        }

        protected void DropDownList1_SelectedIndexChanged1(object sender, EventArgs e)
        {
            DropDownList2.Items.Clear();
            if (DropDownList1.SelectedValue == "福建")
            {
                DropDownList2.Items.Add("龙岩");
                DropDownList2.Items.Add("福州");

            }
            else if (DropDownList1.SelectedValue == "广西")
            {
                DropDownList2.Items.Add("南宁");
                DropDownList2.Items.Add("玉林");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label8.Text = "账号：" + TextBox1.Text + "密码：" + TextBox2.Text + "性别：" + RadioButtonList1.Text + "爱好：" + TextBox3.Text + "籍贯：" + DropDownList1.Text + "市：" + DropDownList2.Text + "其他信息：" + TextBox4.Text;
        }
    }
}