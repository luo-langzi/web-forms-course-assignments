﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Demo1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="注册页面"></asp:Label>
            <br />
            <asp:Label ID="Label2" runat="server" Text="用户名"></asp:Label><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="Label3" runat="server" Text="密码"></asp:Label><asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="Label4" runat="server" Text="性别"></asp:Label><asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem>男</asp:ListItem>
                <asp:ListItem>女</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:Label ID="Label5" runat="server" Text="爱好"></asp:Label><asp:TextBox ID="TextBox2" runat="server" Height="80" Width="200"></asp:TextBox>
            <br />
            省份：<asp:DropDownList ID="DropDownList2" runat="server" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
            <br />
            市区：<asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList>
            <br />
            <asp:Label ID="Label8" runat="server" Text="其它信息"></asp:Label><asp:TextBox ID="TextBox4" runat="server" Height="80" Width="150"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
        </div>
    </form>
</body>
</html>
