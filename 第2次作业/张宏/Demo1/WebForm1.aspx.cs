﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList2.Items.Add("福建");
                DropDownList2.Items.Add("广东");
                DropDownList2.Items.Add("江西");
                DropDownList2.Items.Add("江苏");
                DropDownList2.Items.Add("浙江");
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList3.Items.Clear();
            if (DropDownList2.SelectedValue == "福建")
            {
                DropDownList3.Items.Add("龙岩");
                DropDownList3.Items.Add("泉州");
                DropDownList3.Items.Add("厦门");
                DropDownList3.Items.Add("漳州");
                DropDownList3.Items.Add("三明");
                DropDownList3.Items.Add("莆田");
                DropDownList3.Items.Add("宁德");
            }
            else if(DropDownList2.SelectedValue == "广东")
            {
                DropDownList3.Items.Add("广州");
                DropDownList3.Items.Add("深圳");
                DropDownList3.Items.Add("珠海");
                DropDownList3.Items.Add("佛山");
                DropDownList3.Items.Add("东莞");
                DropDownList3.Items.Add("汕头");
                DropDownList3.Items.Add("韶关");
            }
            else if(DropDownList2.SelectedValue == "江西")
            {
                DropDownList3.Items.Add("南昌");
                DropDownList3.Items.Add("赣州");
                DropDownList3.Items.Add("九江");
                DropDownList3.Items.Add("吉安");
                DropDownList3.Items.Add("井冈山");
                DropDownList3.Items.Add("鹰潭");
                DropDownList3.Items.Add("上饶");
                DropDownList3.Items.Add("瑞金");
                DropDownList3.Items.Add("景德镇");
                DropDownList3.Items.Add("萍乡");
            }
            else if(DropDownList2.SelectedValue == "江苏")
            {
                DropDownList3.Items.Add("苏州");
                DropDownList3.Items.Add("南京");
                DropDownList3.Items.Add("无锡");
            }
            else 
            {
                DropDownList3.Items.Add("杭州");
                DropDownList3.Items.Add("金华");
                DropDownList3.Items.Add("温州");
                DropDownList3.Items.Add("台州");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label6.Text = $"用户名：{TextBox1.Text};性别：{DropDownList1.SelectedValue};爱好:{TextBox2.Text};籍贯：{DropDownList2.SelectedValue}省,{DropDownList3.SelectedValue}市,其它信息：{TextBox4.Text}.";
        }
    }
}