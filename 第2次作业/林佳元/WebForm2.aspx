﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            用户名：<asp:TextBox ID="TextBox1" runat="server">
            <br />
            密码：</asp:Label><asp:TextBox ID="TextBox2" runat="server" TextMode="Password">
            <br />
            性别：<asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem>男</asp:ListItem>
                <asp:ListItem>女</asp:ListItem>
            </asp:DropDownList>
            <br />
            爱好：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <br />
            籍贯：<asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
                <asp:ListItem>浙江</asp:ListItem>
                <asp:ListItem>福建</asp:ListItem>
                <asp:ListItem>北京</asp:ListItem>
                <asp:ListItem>湖南</asp:ListItem>
            </asp:DropDownList>省<asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
            </asp:DropDownList>市
            <br />
            其它信息：<asp:TextBox ID="TextBox4" runat="server" Columns="20" Rows="10" TextMode="MultiLine"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox5" runat="server" TextMode="MultiLine" Rows="10" Columns="20" Enabled="False"></asp:TextBox>
        </div>
    </form>
</body>
</html>
