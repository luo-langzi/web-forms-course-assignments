﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("福建");
                DropDownList1.Items.Add("广东");
                DropDownList1.Items.Add("广西");

            }
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList2.Items.Clear();
            if (DropDownList1.SelectedValue == "福建")
            {
                DropDownList2.Items.Add("莆田");
                DropDownList2.Items.Add("泉州");
                DropDownList2.Items.Add("龙岩");
            }
            else if (DropDownList1.SelectedValue == "广东")
            {
                DropDownList2.Items.Add("广州");
                DropDownList2.Items.Add("深圳");
                DropDownList2.Items.Add("东莞");
            }
            else if (DropDownList1.SelectedValue == "广西")
            {
                DropDownList2.Items.Add("南宁");
                DropDownList2.Items.Add("博白");
                DropDownList2.Items.Add("陆川");

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label1.Text = $"用户名：{TextBox1.Text} 密码：{TextBox2.Text} 性别：{RadioButtonList1.SelectedValue}  爱好：{TextBox3.Text} 籍贯：{DropDownList1.SelectedValue} 其他信息：{TextBox4.Text}";
        }
    }
}