﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.DropDownList1.Items.Add("男");
                this.DropDownList1.Items.Add("女");

                this.DropDownList2.Items.Add("福建");
                this.DropDownList2.Items.Add("湖南");

                this.DropDownList3.Items.Add("漳州");
                this.DropDownList3.Items.Add("宁德");
            }
        }
        protected void DropDownList(object sender, EventArgs e) 
        {
            DropDownList2.Items.Clear();
            if (DropDownList2.SelectedValue == "福建")
            {
                this.DropDownList2.Items.Add("漳州");
                this.DropDownList2.Items.Add("宁德");
            }
            else if(DropDownList2.SelectedValue == "湖南")
            {
                this.DropDownList2.Items.Add("长沙市");
                this.DropDownList2.Items.Add("岳阳市");
            }
        }
        
        protected void Button1_Click(object sender, EventArgs e)
        {
            Label9.Text = $"你的姓名为:{TextBox1.Text} 你的密码是：{TextBox2.Text} 你的性别是:{DropDownList1.Text} 你的爱好是:{TextBox4.Text} 你的籍贯是:{DropDownList2.Text} 其它信息:{TextBox3.Text} ";
        }
    }
}