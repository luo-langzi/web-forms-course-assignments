﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication2.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="用户名："></asp:Label><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="Label2" runat="server" Text="密 码："></asp:Label><asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            <asp:Label ID="Label3" runat="server" Text="性  别："></asp:Label>
             <asp:RadioButton ID="RadioButton1" runat="server" Text="男" />
            <asp:RadioButton ID="RadioButton2" runat="server" Text="女" />
            <br />
            <asp:Label ID="Label4" runat="server" Text="爱  好："></asp:Label><asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="Label5" runat="server" Text="籍  贯："></asp:Label>
            <asp:DropDownList ID="DropDownList1" runat="server"  AutoPostBack="True" OnTextChanged="DropDownList1_TextChanged">
                <asp:ListItem>福建</asp:ListItem>
                <asp:ListItem>广东</asp:ListItem>
                <asp:ListItem>河南</asp:ListItem>
                <asp:ListItem></asp:ListItem>
            </asp:DropDownList>
            省<asp:DropDownList ID="DropDownList2" runat="server">
                <asp:ListItem>厦门</asp:ListItem>
                <asp:ListItem>福州</asp:ListItem>
                <asp:ListItem>漳州</asp:ListItem>
            </asp:DropDownList>
            市<br />
            <asp:Label ID="Label6" runat="server" Text="其他信息："></asp:Label>
            
            <asp:TextBox ID="TextBox5" runat="server" TextMode="MultiLine"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            <asp:TextBox ID="TextBox6" runat="server" TextMode="MultiLine" Height="93px" Width="231px"></asp:TextBox>
        </div>
    </form>
</body>
</html>
