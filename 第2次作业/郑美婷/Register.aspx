﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="WebApplication1.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
<%--    写一个注册界面
用户名：
密码：不能明文显示
性别：只能单选
爱好：
籍贯：省  市
其它信息：多行文本框
提交按钮

提交后，在下方显示提交的数据。--%>
    <form id="form1" runat="server">
        <div>
            用户名：<asp:TextBox ID="TextBox1" runat="server" style="margin-left: 6px"></asp:TextBox><br/>
            密码：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password" style="margin-left: 6px" Width="178px"></asp:TextBox><br/>
            性别：<asp:RadioButtonList ID="RadioButtonList1" runat="server">
                <asp:ListItem>男</asp:ListItem>
                <asp:ListItem>女</asp:ListItem>
            </asp:RadioButtonList><br/>
            爱好：<asp:CheckBoxList ID="CheckBoxList1" runat="server">
                <asp:ListItem>唱歌</asp:ListItem>
                <asp:ListItem>跳舞</asp:ListItem>
                <asp:ListItem>绘画</asp:ListItem>
                <asp:ListItem>打篮球</asp:ListItem>
                <asp:ListItem>打乒乓球</asp:ListItem>
                <asp:ListItem>做作业</asp:ListItem>
                <asp:ListItem>敲代码</asp:ListItem>
                <asp:ListItem>其他</asp:ListItem>
            </asp:CheckBoxList><br/>
            籍贯：
            <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="true">
            <%--    <asp:ListItem>福建</asp:ListItem>
                <asp:ListItem>湖南</asp:ListItem>
                <asp:ListItem>黑龙江</asp:ListItem>--%>
            </asp:DropDownList>省
            <asp:DropDownList ID="DropDownList2" runat="server">
          <%--      <asp:ListItem>福州</asp:ListItem>
                <asp:ListItem>龙岩</asp:ListItem>
                <asp:ListItem>三明</asp:ListItem>
                <asp:ListItem>长沙</asp:ListItem>
                <asp:ListItem>衡阳</asp:ListItem>
                <asp:ListItem>岳阳</asp:ListItem>
                <asp:ListItem>哈尔滨</asp:ListItem>
                <asp:ListItem>齐齐哈尔</asp:ListItem>
                <asp:ListItem>大庆</asp:ListItem>--%>
            </asp:DropDownList>市<br/><br/>
            其他信息：<asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine"></asp:TextBox><br/><br/>
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" style="margin-left: 235px" Width="67px"/><br/>
            <asp:Literal ID="Literal0" runat="server"></asp:Literal><br/>
            <asp:Literal ID="Literal1" runat="server"></asp:Literal><br/>
            <asp:Literal ID="Literal2" runat="server"></asp:Literal><br/>
            <asp:Literal ID="Literal3" runat="server"></asp:Literal><br/>
            <asp:Literal ID="Literal4" runat="server"></asp:Literal><br/>
            <asp:Literal ID="Literal5" runat="server"></asp:Literal><br/>
        </div>
    </form>
</body>
</html>
