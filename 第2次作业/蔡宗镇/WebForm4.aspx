﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm4.aspx.cs" Inherits="WebApplication1.WebForm4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="用户名："></asp:Label>&nbsp&nbsp&nbsp&nbsp<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Text="密码：" EnableViewState="True"></asp:Label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label3" runat="server" Text="确认密码："></asp:Label><asp:TextBox ID="TextBox3" runat="server" TextMode="Password" ToolTip="请再次确认密码"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label4" runat="server" Text="邮箱："></asp:Label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:TextBox ID="TextBox4" runat="server" TextMode="Email"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label5" runat="server" Text="地址："></asp:Label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:TextBox ID="TextBox5" runat="server" Visible="True" TextMode="MultiLine" Rows="5" Width="300px" TabIndex="10"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="注册" OnClick="Button1_Click" style="height: 21px" />

            <br />
            <br />
            <asp:Label ID="Label6" runat="server" Text="管理员提示：" Font-Size="X-Large" ForeColor="#CC3300"></asp:Label>
            <br />
            <asp:TextBox ID="TextBox6" runat="server" EnableViewState="True" Enabled="False" Text="你的信息将会被保护" TextMode="MultiLine" Rows="10" Width="300"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label7" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
