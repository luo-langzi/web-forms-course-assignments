﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm5.aspx.cs" Inherits="WebApplication1.WebForm5" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="注册页面" Font-Size="XX-Large" ForeColor="#CC3300"></asp:Label>
             <br />
             <br />
            <asp:Label ID="Label2" runat="server" Text="用户名："></asp:Label>&nbsp&nbsp&nbsp&nbsp<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label3" runat="server" Text="密码：" EnableViewState="True"></asp:Label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label4" runat="server" Text="确认密码："></asp:Label><asp:TextBox ID="TextBox3" runat="server" TextMode="Password" ToolTip="请再次确认密码"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label5" runat="server" Text="性别："></asp:Label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:RadioButtonList ID="RadioButtonList1" runat="server">
            <asp:ListItem>男</asp:ListItem>
            <asp:ListItem>女</asp:ListItem>
            </asp:RadioButtonList>            
           
            <asp:Label ID="Label6" runat="server" Text="爱好："></asp:Label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:TextBox ID="TextBox4" runat="server" TextMode="MultiLine"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label7" runat="server" Text="籍贯:"></asp:Label><asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged1"></asp:DropDownList>
                <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
            <br />
            <br />
            <asp:Label ID="Label8" runat="server" Text="其他信息："></asp:Label><asp:TextBox ID="TextBox5" runat="server" Columns="100" Height="40"></asp:TextBox>
            <br />
            <br />
             <asp:Button ID="Button1" runat="server" Text="提交" /></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label9" runat="server" OnDataBinding="Label9_DataBinding"></asp:Label>
        </div>
    </form>
</body>
</html>
