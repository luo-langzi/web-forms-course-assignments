﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (PreviousPage !=null)
            {
                TextBox tb = (TextBox)Page.PreviousPage.FindControl("TextBox1");
                if (tb !=null)
                {
                    Label1.Text ="WEBFROM02:"+ tb.Text;
                }
            }
        }
    }
}