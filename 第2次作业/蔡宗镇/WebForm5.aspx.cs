﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm5 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DropDownList2.Items.Add("福建省");
                DropDownList2.Items.Add("广东省");
                DropDownList2.Items.Add("江西省");
                DropDownList2.Items.Add("浙江省");
            }
           
            }
        protected void DropDownList2_SelectedIndexChanged1(object sender, EventArgs e)
        {
            DropDownList2.Items.Clear();
            if (DropDownList2.SelectedValue=="福建省")
            {
                DropDownList1.Items.Add("厦门市");
                DropDownList1.Items.Add("泉州市");
                DropDownList1.Items.Add("漳州市");
            }
            else if (DropDownList2.SelectedValue == "广东省")
            {
                DropDownList1.Items.Add("深圳市");
                DropDownList1.Items.Add("广州市");
                DropDownList1.Items.Add("珠江市");
            }
            else if (DropDownList2.SelectedValue == "江西省")
            {
                DropDownList1.Items.Add("上饶市");
                DropDownList1.Items.Add("南昌市");
                DropDownList1.Items.Add("九江市");
            }
            else 
            {
                DropDownList1.Items.Add("义乌市");
                DropDownList1.Items.Add("宁波市");
                DropDownList1.Items.Add("杭州市");
            }
        }
        protected void Label9_DataBinding(object sender, EventArgs e)
        {
            Label9.Text = $"用户名：{TextBox1.Text}&nbsp&nbsp&nbsp性别：{RadioButtonList1.SelectedValue}&nbsp&nbsp&nbsp爱好：{TextBox4.Text}&nbsp&nbsp&nbsp籍贯：{DropDownList2.SelectedValue}{DropDownList1.SelectedValue}";
        }

        protected void DropDownList2_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }
    }
}