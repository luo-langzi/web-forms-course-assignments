﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["Mycount"]= 0;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            int count = (int)ViewState["Mycount"];
            count++;
            Label1.Text = "你点击了" + count + "次";
            ViewState["Mycount"] = count;
        }
    }
}