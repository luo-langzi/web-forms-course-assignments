﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["a"] = 0;
            }
        }

   

        protected void Button1_Click1(object sender, EventArgs e)
        {
           
               
            
            int count = Convert.ToInt32(ViewState["a"]);
            count++;
            Label2.Text = $"单机按钮了{count}次";
            ViewState["a"] = count;

        }


        protected void Button2_Click1(object sender, EventArgs e)
        {
            ViewState["a"] = Label2.Text;
        }

        protected void Button3_Click1(object sender, EventArgs e)
        {
            Label3.Text = ViewState["a"].ToString();
        }
    }
}