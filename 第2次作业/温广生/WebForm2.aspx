﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="WebApplication1.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Literal ID="Literal1" runat="server" Text="请输入你的姓名："></asp:Literal>
            <asp:TextBox ID="TextBox1" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <br /> 
            <asp:Label ID="Label1" runat="server" Text="  "></asp:Label>
            <br /> 
            <asp:Button ID="Button1" runat="server" Text="提交到本页" OnClick="TextBox1_TextChanged" />
            <asp:Button ID="Button2" runat="server" Text="提交到page2" PostBackUrl="~/WebForm3.aspx" />
        </div>
    </form>
</body>
</html>
