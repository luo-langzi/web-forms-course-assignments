﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication4
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //if (Request["TextBox2.Text"] != null)
            //{
            //    //TextBox2.Attributes.Add("value", TextBox2.Text);
            //    TextBox2.Attributes["value"] = Request["TextBox2"].ToString();
            //    //DropDownList2.ClearSelection();
            //    //DropDownList2.Items.Clear();
            //    //DropDownList2.SelectedIndex = -1; 
            //}
            if (Request["TextBox2"] != null)
            {
                TextBox2.Attributes["value"] = Request["TextBox2"].ToString();
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList2.Items.Clear();
            if (DropDownList1.Text.Equals("江西"))
            {
                //if (!IsPostBack)
                //{
                //    //DropDownList2.SelectedIndex = -1;
                //    //DropDownList2.Items.Clear();
                //}

                DropDownList2.Items.Add("南昌");
                DropDownList2.Items.Add("九江");
                DropDownList2.Items.Add("赣州");
            }
            else if (DropDownList1.Text.Equals("福建"))
            {
                //DropDownList2.SelectedIndex = -1;
                //DropDownList2.ClearSelection();

                DropDownList2.Items.Add("厦门");
                DropDownList2.Items.Add("泉州");
                DropDownList2.Items.Add("德化");
            }
            else if(DropDownList1.Text.Equals("广东"))
            {
                //DropDownList2.SelectedIndex = -1;
                //DropDownList2.ClearSelection();

                DropDownList2.Items.Add("河源");
                DropDownList2.Items.Add("梅州");
                DropDownList2.Items.Add("惠州");
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            TextBox5.Text = $"用户名：{TextBox1.Text}\r\n爱好：{TextBox3.Text}\r\n籍贯：{DropDownList1.Text}省{DropDownList2.Text}\r\n其他信息：{TextBox4.Text}";
        }
    }
}