﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("福建");
                DropDownList1.Items.Add("江西");
                DropDownList1.Items.Add("浙江");
            }
            
        }

        protected void DropDownList1_TextChanged(object sender, EventArgs e)
        {
            DropDownList2.Items.Clear();
            if (DropDownList1.SelectedValue == "福建")
            {
                DropDownList2.Items.Add("龙岩");
                DropDownList2.Items.Add("厦门");
                DropDownList2.Items.Add("福州");
                DropDownList2.Items.Add("漳州");
            }
            else if (DropDownList1.SelectedValue == "江西")
            {
                DropDownList2.Items.Add("赣州");
                DropDownList2.Items.Add("南康");
                DropDownList2.Items.Add("南昌");
            }
            else if (DropDownList1.SelectedValue=="河南")
            {
                DropDownList2.Items.Add("焦作");
                DropDownList2.Items.Add("郑州");
                DropDownList2.Items.Add("洛阳");
                DropDownList2.Items.Add("驻马店");

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            TextBox5.Text = "姓名:" + TextBox1.Text + "\n" + "年龄:" + TextBox2.Text + "\n" + "性别:" + RadioButtonList1.Text + "爱好:" + TextBox3.Text + "省:" + DropDownList1.Text + "市:" + DropDownList2.Text + "其他信息:" + TextBox4.Text;
        }
    }
}