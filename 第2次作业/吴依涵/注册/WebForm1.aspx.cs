﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication4
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Add("广东省");
                DropDownList1.Items.Add("福建省");
                DropDownList1.Items.Add("江西省");
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList2.Items.Clear();
            if (DropDownList1.SelectedValue == "福建省")
            {
                DropDownList2.Items.Add("莆田市");
                DropDownList2.Items.Add("福州市");
                DropDownList2.Items.Add("龙岩市");
            }
            else if (DropDownList1.SelectedValue == "广东省")
            {
                DropDownList2.Items.Add("深圳市");
                DropDownList2.Items.Add("东莞市");
                DropDownList2.Items.Add("宝安市");
            }
            else if (DropDownList1.SelectedValue == "江西省")
            {
                DropDownList2.Items.Add("赣州市");
                DropDownList2.Items.Add("上饶市");
                DropDownList2.Items.Add("南昌市");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label1.Text = $"用户名：{TextBox1.Text}\r\n密码：{TextBox2.Text}\r\n性别：{RadioButtonList1.SelectedValue}\r\n爱好：{TextBox3.Text}\r\n籍贯：{DropDownList1.SelectedValue}{DropDownList2.SelectedValue}\r\n其他信息：{TextBox4.Text}";
        }
    }
}