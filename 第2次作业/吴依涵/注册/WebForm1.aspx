﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication4.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            用户名：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            性别：<asp:RadioButtonList ID="RadioButtonList1" runat="server">
                <asp:ListItem>男</asp:ListItem>
                <asp:ListItem>女</asp:ListItem>
            </asp:RadioButtonList>
            <br />
            爱好：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <br />
            籍贯：<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
<%--                <asp:ListItem>广东省</asp:ListItem>
                <asp:ListItem>福建省</asp:ListItem>
                <asp:ListItem>江西省</asp:ListItem>--%>
            </asp:DropDownList>
            <br />
            <asp:DropDownList ID="DropDownList2" runat="server">
<%--                <asp:ListItem>深圳市</asp:ListItem>
                <asp:ListItem>东莞市</asp:ListItem>
                <asp:ListItem>宝安市</asp:ListItem>
                <asp:ListItem>莆田市</asp:ListItem>
                <asp:ListItem>福州市</asp:ListItem>
                <asp:ListItem>龙岩市</asp:ListItem>
                <asp:ListItem>赣州市</asp:ListItem>
                <asp:ListItem>上饶市</asp:ListItem>
                <asp:ListItem>南昌市</asp:ListItem>--%>
            </asp:DropDownList>
            <br />
            其他信息：<asp:TextBox ID="TextBox4" runat="server" Height="84px" TextMode="MultiLine" Width="206px"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
