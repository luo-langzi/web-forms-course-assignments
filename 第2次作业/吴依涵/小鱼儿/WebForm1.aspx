﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication3.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server" aria-autocomplete="inline">
        <div>
            <asp:Literal ID="Literal1" runat="server" Text="用户名："></asp:Literal><asp:TextBox ID="TextBox1" runat="server" style="margin-left: 16px" Width="141px"></asp:TextBox>
            <br />
            <asp:Label ID="Label1" runat="server" Text="密码："></asp:Label><asp:TextBox ID="TextBox2" runat="server" style="margin-left: 32px" Width="141px" TextMode="Password"></asp:TextBox>
            <br />
            <asp:Label ID="Label2" runat="server" Text="确认密码："></asp:Label><asp:TextBox ID="TextBox3" runat="server" Width="141px" TextMode="Password"></asp:TextBox>
            <br />
            <asp:Literal ID="Literal2" runat="server" Text="邮箱："></asp:Literal><asp:TextBox ID="TextBox4" runat="server" style="margin-left: 32px" Width="141px"></asp:TextBox>
            <br />
            <asp:Literal ID="Literal3" runat="server" Text="详细地址："></asp:Literal><asp:TextBox ID="TextBox5" runat="server" Rows="20" Height="17px" Width="141px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="注册" OnClick="Button1_Click"/>
            <br />
            <br />
            管理员提示
            <br />
            <asp:TextBox ID="TextBox6" runat="server" Enabled="False" TextMode="MultiLine" Rows="10" Columns="30">你的信息已被记录！</asp:TextBox>
            <br />
            <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
