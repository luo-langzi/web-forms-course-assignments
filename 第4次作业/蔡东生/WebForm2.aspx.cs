﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication3
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string name = Request.Form["TextBox1"];
            string pass = Request.Form["TextBox2"];

            string name1 = Request.Params["TextBox1"];
            string pass1 = Request.Params["TextBox2"];

            string name3 = "";
            string pass3 = "";
            if (PreviousPage != null)
            {
                TextBox name2 = (TextBox)PreviousPage.FindControl("TextBox1");
                TextBox pass2 = (TextBox)PreviousPage.FindControl("TextBox2");
                if (name2 != null)
                {
                    name3 = name2.Text;
                }
                if (pass2 != null)
                {
                    pass3 = pass2.Text;
                }
            }
            string name4 = Request.QueryString["userName"];
            string pass4 = Request.QueryString["pwd"];

            Response.Write("Request.Form  ==>  " + name + "<br/>");
            Response.Write("Request.Form  ==>  " + pass + "<br/><br/>");

            Response.Write("Request.Params  ==>  " + name1 + "<br/>");
            Response.Write("Request.Params  ==>  " + pass1 + "<br/><br/>");

            Response.Write("Request.QueryString  ==>  " + name4 + "<br/>");
            Response.Write("Request.QueryString  ==>  " + pass4 + "<br/><br/>");

            Response.Write("PreviousPage  ==>  " + name3 + "<br/>");
            Response.Write("PreviousPage  ==>  " + pass3 + "<br/><br/><br/><br/>");
        }
    }
}