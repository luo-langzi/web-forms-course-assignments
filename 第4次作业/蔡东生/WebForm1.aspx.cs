﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication3
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            string userName = TextBox1.Text;
            string pwd = TextBox2.Text;

            //get方式发送请求，就是把参数放在url里
            //请求转发
            Response.Redirect($"WebForm2.aspx?userName={TextBox1.Text}&pwd={TextBox2.Text}");
        }
    }
}