﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server" method="post" action="WebForm2.aspx">
        <div>
            用户名：<asp:TextBox ID="Username" runat="server"></asp:TextBox>
            <br />
            密码：<asp:TextBox ID="Password" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" Text="post请求" PostBackUrl="~/WebForm2.aspx" /><asp:Button ID="Button2" runat="server" Text="get请求" OnClick="Button2_Click"  />
        </div>
    </form>
</body>
</html>
