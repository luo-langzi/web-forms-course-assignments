﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string username = Request.Form["Username"];
            string password = Request.Form["Password"];

            string username1 = Request.Params["Username"];
            string password1 = Request.Params["Password"];

            string username2 = Request.QueryString["Username"];
            string password2 = Request.QueryString["Password"];

            string username3 = "";
            string password3 = "";
            if (PreviousPage != null)
            {
                TextBox userNameBox = (TextBox)PreviousPage.FindControl("Username");
                TextBox pwdNameBox = (TextBox)PreviousPage.FindControl("Password");
                if (userNameBox != null)
                {
                    username3 = userNameBox.Text;
                }
                if (pwdNameBox != null)
                {
                    password3 = pwdNameBox.Text;
                }
            }


            Response.Write("Form==>" + $"<h1>登录成功，欢迎{username}，您的密码是{password}</h1>");
            Response.Write("Params==>" + $"<h1>登录成功，欢迎{username1}，您的密码是{password1}</h1>");
            Response.Write("QueryString==>" + $"<h1>登录成功，欢迎{username2}，您的密码是{password2}</h1>");
            Response.Write("PreviousPage==>" + $"<h1>登录成功，欢迎{username3}，您的密码是{password3}</h1>");
        }
    }
}