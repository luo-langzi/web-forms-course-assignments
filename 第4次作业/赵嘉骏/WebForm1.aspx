﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication3.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server" method="post" action="WebForm2.aspx">
        <div>
             用户名：<asp:TextBox ID="TextBox1" runat="server"  ></asp:TextBox>
            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="用户名不能为admin" ControlToValidate="TextBox1" Display="Dynamic" OnServerValidate="CustomValidator1_ServerValidate" ></asp:CustomValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="用户名不能为空！" ControlToValidate="TextBox1" Font-Size="Large" ForeColor="Red" Font-Bold="true" Display="Dynamic"></asp:RequiredFieldValidator>
            <br />
            密码：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="密码不能为空！" ControlToValidate="TextBox2" Display="Dynamic" Font-Bold="true" Font-Size="Large" ForeColor="Red"></asp:RequiredFieldValidator>
            <br />
            确认密码：<asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="请再次输入密码！" ControlToValidate="TextBox3"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次密码不一致！" ControlToCompare="TextBox2" ControlToValidate="TextBox3"></asp:CompareValidator>
            <br />
            <asp:Button ID="Button1" runat="server" Text="登录（POST）" PostBackUrl="~/WebForm2.aspx" />
            <br />
            <asp:Button ID="Button2" runat="server" Text="登录（GET）" OnClick="Button2_Click" />
            
        </div>
    </form>
</body>
</html>
