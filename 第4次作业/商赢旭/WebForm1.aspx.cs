﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication3
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        int a;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string userName = UserName.Text;
            string pwd = Pwd.Text;
            Response.Redirect($"WebForm2.aspx?userName={userName}&pwd={pwd}");
           
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string userName1 = UserName.Text;
            string pwd1 = Pwd.Text;
            Response.Redirect($"WebForm3.aspx?userName={userName1}&pwd={pwd1}");
        }
    }
}