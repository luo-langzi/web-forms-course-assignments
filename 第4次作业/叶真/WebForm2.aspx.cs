﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string userName = Request.Form["TextBox1"];
            string password = Request.Form["TextBox2"];

            Response.Write($"<h3>Form:欢迎你，{userName},您的密码是：{password}</h3>");

            string userName2 = Request.QueryString["UserName"];
            string password2 = Request.QueryString["PassWord"];

            Response.Write($"<h3>QueryString:欢迎你，{userName2},您的密码是：{password2}</h3>");

            string userName3 = Request.Params["TextBox1"];
            string password3 = Request.Params["TextBox2"];

            Response.Write($"<h3>Params:欢迎你，{userName3},您的密码是：{password3}</h3>");

            string userName4 = "";
            string password4 = "";
            if (PreviousPage != null)
            {
                TextBox UserNameBox = (TextBox)PreviousPage.FindControl("TextBox1");
                TextBox PasswordBox = (TextBox)PreviousPage.FindControl("TextBox2");
                if (UserNameBox != null)
                {
                    userName4 = UserNameBox.Text;
                }
                if (PasswordBox != null)
                {
                    password4 = PasswordBox.Text;
                }
            }
            Response.Write($"<h3>PerviousPage:欢迎你，{userName4},您的密码是：{password4}</h3>");
        }
    }
}