﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication11
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string UserName1 = Request.Form["UserName"];
            string Password1 = Request.Form["Password"];

            string UserName2 = Request.Params["UserName"];
            string Password2 = Request.Params["Password"];

            string UserName3 = Request.QueryString["UserName"];
            string Password3 = Request.QueryString["Password"];

            string UserName4 = "";
            string Password4 = "";

            if (PreviousPage != null)
            {
                TextBox textBox = (TextBox)PreviousPage.FindControl("UserName");
                TextBox textBox1 = (TextBox)PreviousPage.FindControl("Password");
                if (UserName1 != null)
                {
                    UserName4 = textBox.Text;
                }
                if (UserName1 != null)
                {
                    Password4 = textBox1.Text;
                }
            }


            Response.Write($"<h1>From 用户名：{UserName1} 密码：{Password1}</h1>");
            Response.Write($"<h1>Params 用户名：{UserName2} 密码：{Password2}</h1>");
            Response.Write($"<h1>QueryString 用户名：{UserName3} 密码：{Password3}</h1>");
            Response.Write($"<h1>PreviousPage 用户名：{UserName4} 密码：{Password4}</h1>");
        }
    }
}