﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string a1 = Request.Form["TextBox1"];
            string b1 = Request.Form["TextBox2"];

            Response.Write($"Post <h1>欢迎你，{a1}，你的密码为{b1}</h1>");


            string a2 = Request.QueryString["a"];
            string b2 = Request.QueryString["b"];
            Response.Write($"Get <h1>欢迎你，{a2}，你的密码为{b2}</h1>");
        }
    }
}