﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write("Request.RequestType 获取你的提交方式 ==> " + Request.RequestType + "<br/>");
            string userName2 = Request.QueryString["UserName"];
            string pwd2 = Request.QueryString["Pwd"];
            Response.Write("Get里的 Request.QueryString  ==>  " + userName2 + "<br/>");
            Response.Write("Request.QueryString  ==>  " + pwd2 + "<br/><br/>");

            string userName = Request.Form["UserName"];
            string pwd = Request.Form["Pwd"];
            Response.Write("Post里的 Request.Form  ==>  " + userName + "<br/>");
            Response.Write("Request.Form  ==>  " + pwd + "<br/>");
        }
    }
}