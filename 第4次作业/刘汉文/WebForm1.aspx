﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication2.WebForm1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server" action="WebForm2.aspx">
        <div>
            <b>登陆页面</b><br />
             用户名:<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><br />
             密&nbsp;码:<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox><br />
            <asp:Button ID="Button1" runat="server" Text="get按钮" PostBackUrl="~/WebForm2.aspx"/>&nbsp;&nbsp;
            <asp:Button ID="Button2" runat="server" Text="pose按钮" OnClick="Button2_Click" />
        </div>
    </form>
</body>
</html>
