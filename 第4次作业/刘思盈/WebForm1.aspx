﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication7.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
         <div>
            <%--            写一个登录页面，有用户名和密码，点击登录，跳转到登录成功页面，
登录成功页面显示“欢迎你，张三，你的密码是：123”(h1效果)

登录按钮有两个，一个是Post请求，一个是Get请求。
在登录成功页面，需判断请求的方式，如果是Post，请用Post对应的方式获取Request数据，
            如果是Get，请用Get对应的方式获取Request数据。--%>
            用户名：<asp:TextBox ID="UserName" runat="server"></asp:TextBox><br />
            密码：<asp:TextBox ID="Pwd" runat="server"></asp:TextBox><br />
            <asp:Button ID="Button1" runat="server" Text="Post登录" PostBackUrl="~/WebForm2.aspx"/>
            <asp:Button ID="Button2" runat="server" Text="登录2" OnClick="Button2_Click" />
     
        </div>
    </form>
</body>
</html>
