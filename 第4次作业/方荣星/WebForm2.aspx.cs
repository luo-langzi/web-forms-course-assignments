﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication7
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string userName = Request.Form["UserName"];
            string pwd = Request.Form["Pwd"];

            string userName1 = Request.Params["UserName"];
            string pwd1 = Request.Params["Pwd"];

            string userName2 = Request.QueryString["UserName"];
            string pwd2 = Request.QueryString["Pwd"];

            string userName3 = "";
            string pwd3 = "";
            if (PreviousPage != null)
            {
                TextBox userNameBox = (TextBox)PreviousPage.FindControl("UserName");
                TextBox pwdNameBox = (TextBox)PreviousPage.FindControl("Pwd");
                if (userName != null)
                {
                    userName3 = userNameBox.Text;
                }
                if (pwdNameBox != null)
                {
                    pwd3 = pwdNameBox.Text;
                }
            }

            Response.Write("Request.Form  ==>  " + userName + "<br/>");
            Response.Write("Request.Form  ==>  " + pwd + "<br/><br/>");

            Response.Write("Request.Params  ==>  " + userName1 + "<br/>");
            Response.Write("Request.Params  ==>  " + pwd1 + "<br/><br/>");

            Response.Write("Request.QueryString  ==>  " + userName2 + "<br/>");
            Response.Write("Request.QueryString  ==>  " + pwd2 + "<br/><br/>");

            Response.Write("PreviousPage  ==>  " + userName3 + "<br/>");
            Response.Write("PreviousPage  ==>  " + pwd3 + "<br/><br/><br/><br/>");

            string nameQueryString = Request.QueryString["name"];
            string ageQueryString = Request.QueryString["age"];

            Response.Write("Request.QueryString['name'] ==> " + nameQueryString + "<br/>");
            Response.Write("Request.QueryString['age'] ==> " + ageQueryString + "<br/>");
        }

    }
}