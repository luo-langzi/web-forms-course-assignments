﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server" action="WebForm2.aspx" method="get" >
        <div>
            用户名：<asp:TextBox ID="UserName" runat="server"></asp:TextBox>
            <br />
            密 码 ：<asp:TextBox ID="Pwd" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" Text="Get请求" OnClick="Button1_Click" />
            <asp:Button ID="Button2" runat="server" Text="Post请求" OnClick="Button1_Click"  PostBackUrl="~/WebForm2.aspx"/>
        </div>
    </form>
</body>
</html>
