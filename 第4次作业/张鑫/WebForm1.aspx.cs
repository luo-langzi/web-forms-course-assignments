﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string textbox1 = TextBox1.Text;
            string textbox2 = TextBox2.Text;

            Response.Redirect($"WebForm2.aspx?textbox1={textbox1}&textbox2={textbox2}");
        }
    }
}